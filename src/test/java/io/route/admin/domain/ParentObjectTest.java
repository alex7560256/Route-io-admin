package io.route.admin.domain;

import static org.assertj.core.api.Assertions.assertThat;

import io.route.admin.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class ParentObjectTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(ParentObject.class);
        ParentObject parentObject1 = new ParentObject();
        parentObject1.setId(1L);
        ParentObject parentObject2 = new ParentObject();
        parentObject2.setId(parentObject1.getId());
        assertThat(parentObject1).isEqualTo(parentObject2);
        parentObject2.setId(2L);
        assertThat(parentObject1).isNotEqualTo(parentObject2);
        parentObject1.setId(null);
        assertThat(parentObject1).isNotEqualTo(parentObject2);
    }
}
