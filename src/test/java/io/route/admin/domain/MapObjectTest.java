package io.route.admin.domain;

import static org.assertj.core.api.Assertions.assertThat;

import io.route.admin.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MapObjectTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MapObject.class);
        MapObject mapObject1 = new MapObject();
        mapObject1.setId(1L);
        MapObject mapObject2 = new MapObject();
        mapObject2.setId(mapObject1.getId());
        assertThat(mapObject1).isEqualTo(mapObject2);
        mapObject2.setId(2L);
        assertThat(mapObject1).isNotEqualTo(mapObject2);
        mapObject1.setId(null);
        assertThat(mapObject1).isNotEqualTo(mapObject2);
    }
}
