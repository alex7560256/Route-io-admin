package io.route.admin.domain;

import static org.assertj.core.api.Assertions.assertThat;

import io.route.admin.web.rest.TestUtil;
import org.junit.jupiter.api.Test;

class MapObjectTypeTest {

    @Test
    void equalsVerifier() throws Exception {
        TestUtil.equalsVerifier(MapObjectType.class);
        MapObjectType mapObjectType1 = new MapObjectType();
        mapObjectType1.setId(1L);
        MapObjectType mapObjectType2 = new MapObjectType();
        mapObjectType2.setId(mapObjectType1.getId());
        assertThat(mapObjectType1).isEqualTo(mapObjectType2);
        mapObjectType2.setId(2L);
        assertThat(mapObjectType1).isNotEqualTo(mapObjectType2);
        mapObjectType1.setId(null);
        assertThat(mapObjectType1).isNotEqualTo(mapObjectType2);
    }
}
