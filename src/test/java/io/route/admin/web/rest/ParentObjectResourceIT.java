package io.route.admin.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.route.admin.IntegrationTest;
import io.route.admin.domain.ParentObject;
import io.route.admin.repository.ParentObjectRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link ParentObjectResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class ParentObjectResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/parent-objects";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private ParentObjectRepository parentObjectRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restParentObjectMockMvc;

    private ParentObject parentObject;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ParentObject createEntity(EntityManager em) {
        ParentObject parentObject = new ParentObject().title(DEFAULT_TITLE).description(DEFAULT_DESCRIPTION);
        return parentObject;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static ParentObject createUpdatedEntity(EntityManager em) {
        ParentObject parentObject = new ParentObject().title(UPDATED_TITLE).description(UPDATED_DESCRIPTION);
        return parentObject;
    }

    @BeforeEach
    public void initTest() {
        parentObject = createEntity(em);
    }

    @Test
    @Transactional
    void createParentObject() throws Exception {
        int databaseSizeBeforeCreate = parentObjectRepository.findAll().size();
        // Create the ParentObject
        restParentObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parentObject)))
            .andExpect(status().isCreated());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeCreate + 1);
        ParentObject testParentObject = parentObjectList.get(parentObjectList.size() - 1);
        assertThat(testParentObject.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testParentObject.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
    }

    @Test
    @Transactional
    void createParentObjectWithExistingId() throws Exception {
        // Create the ParentObject with an existing ID
        parentObject.setId(1L);

        int databaseSizeBeforeCreate = parentObjectRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restParentObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parentObject)))
            .andExpect(status().isBadRequest());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = parentObjectRepository.findAll().size();
        // set the field null
        parentObject.setTitle(null);

        // Create the ParentObject, which fails.

        restParentObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parentObject)))
            .andExpect(status().isBadRequest());

        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllParentObjects() throws Exception {
        // Initialize the database
        parentObjectRepository.saveAndFlush(parentObject);

        // Get all the parentObjectList
        restParentObjectMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(parentObject.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)));
    }

    @Test
    @Transactional
    void getParentObject() throws Exception {
        // Initialize the database
        parentObjectRepository.saveAndFlush(parentObject);

        // Get the parentObject
        restParentObjectMockMvc
            .perform(get(ENTITY_API_URL_ID, parentObject.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(parentObject.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION));
    }

    @Test
    @Transactional
    void getNonExistingParentObject() throws Exception {
        // Get the parentObject
        restParentObjectMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewParentObject() throws Exception {
        // Initialize the database
        parentObjectRepository.saveAndFlush(parentObject);

        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();

        // Update the parentObject
        ParentObject updatedParentObject = parentObjectRepository.findById(parentObject.getId()).get();
        // Disconnect from session so that the updates on updatedParentObject are not directly saved in db
        em.detach(updatedParentObject);
        updatedParentObject.title(UPDATED_TITLE).description(UPDATED_DESCRIPTION);

        restParentObjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedParentObject.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedParentObject))
            )
            .andExpect(status().isOk());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
        ParentObject testParentObject = parentObjectList.get(parentObjectList.size() - 1);
        assertThat(testParentObject.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testParentObject.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void putNonExistingParentObject() throws Exception {
        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();
        parentObject.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParentObjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, parentObject.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(parentObject))
            )
            .andExpect(status().isBadRequest());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchParentObject() throws Exception {
        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();
        parentObject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParentObjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(parentObject))
            )
            .andExpect(status().isBadRequest());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamParentObject() throws Exception {
        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();
        parentObject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParentObjectMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(parentObject)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateParentObjectWithPatch() throws Exception {
        // Initialize the database
        parentObjectRepository.saveAndFlush(parentObject);

        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();

        // Update the parentObject using partial update
        ParentObject partialUpdatedParentObject = new ParentObject();
        partialUpdatedParentObject.setId(parentObject.getId());

        partialUpdatedParentObject.description(UPDATED_DESCRIPTION);

        restParentObjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedParentObject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedParentObject))
            )
            .andExpect(status().isOk());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
        ParentObject testParentObject = parentObjectList.get(parentObjectList.size() - 1);
        assertThat(testParentObject.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testParentObject.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void fullUpdateParentObjectWithPatch() throws Exception {
        // Initialize the database
        parentObjectRepository.saveAndFlush(parentObject);

        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();

        // Update the parentObject using partial update
        ParentObject partialUpdatedParentObject = new ParentObject();
        partialUpdatedParentObject.setId(parentObject.getId());

        partialUpdatedParentObject.title(UPDATED_TITLE).description(UPDATED_DESCRIPTION);

        restParentObjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedParentObject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedParentObject))
            )
            .andExpect(status().isOk());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
        ParentObject testParentObject = parentObjectList.get(parentObjectList.size() - 1);
        assertThat(testParentObject.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testParentObject.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
    }

    @Test
    @Transactional
    void patchNonExistingParentObject() throws Exception {
        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();
        parentObject.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restParentObjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, parentObject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(parentObject))
            )
            .andExpect(status().isBadRequest());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchParentObject() throws Exception {
        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();
        parentObject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParentObjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(parentObject))
            )
            .andExpect(status().isBadRequest());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamParentObject() throws Exception {
        int databaseSizeBeforeUpdate = parentObjectRepository.findAll().size();
        parentObject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restParentObjectMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(parentObject))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the ParentObject in the database
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteParentObject() throws Exception {
        // Initialize the database
        parentObjectRepository.saveAndFlush(parentObject);

        int databaseSizeBeforeDelete = parentObjectRepository.findAll().size();

        // Delete the parentObject
        restParentObjectMockMvc
            .perform(delete(ENTITY_API_URL_ID, parentObject.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<ParentObject> parentObjectList = parentObjectRepository.findAll();
        assertThat(parentObjectList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
