package io.route.admin.web.rest;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.route.admin.IntegrationTest;
import io.route.admin.domain.MapObjectType;
import io.route.admin.repository.MapObjectTypeRepository;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;

/**
 * Integration tests for the {@link MapObjectTypeResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MapObjectTypeResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String ENTITY_API_URL = "/api/map-object-types";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MapObjectTypeRepository mapObjectTypeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMapObjectTypeMockMvc;

    private MapObjectType mapObjectType;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MapObjectType createEntity(EntityManager em) {
        MapObjectType mapObjectType = new MapObjectType().title(DEFAULT_TITLE);
        return mapObjectType;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MapObjectType createUpdatedEntity(EntityManager em) {
        MapObjectType mapObjectType = new MapObjectType().title(UPDATED_TITLE);
        return mapObjectType;
    }

    @BeforeEach
    public void initTest() {
        mapObjectType = createEntity(em);
    }

    @Test
    @Transactional
    void createMapObjectType() throws Exception {
        int databaseSizeBeforeCreate = mapObjectTypeRepository.findAll().size();
        // Create the MapObjectType
        restMapObjectTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObjectType)))
            .andExpect(status().isCreated());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeCreate + 1);
        MapObjectType testMapObjectType = mapObjectTypeList.get(mapObjectTypeList.size() - 1);
        assertThat(testMapObjectType.getTitle()).isEqualTo(DEFAULT_TITLE);
    }

    @Test
    @Transactional
    void createMapObjectTypeWithExistingId() throws Exception {
        // Create the MapObjectType with an existing ID
        mapObjectType.setId(1L);

        int databaseSizeBeforeCreate = mapObjectTypeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMapObjectTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObjectType)))
            .andExpect(status().isBadRequest());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = mapObjectTypeRepository.findAll().size();
        // set the field null
        mapObjectType.setTitle(null);

        // Create the MapObjectType, which fails.

        restMapObjectTypeMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObjectType)))
            .andExpect(status().isBadRequest());

        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMapObjectTypes() throws Exception {
        // Initialize the database
        mapObjectTypeRepository.saveAndFlush(mapObjectType);

        // Get all the mapObjectTypeList
        restMapObjectTypeMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mapObjectType.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)));
    }

    @Test
    @Transactional
    void getMapObjectType() throws Exception {
        // Initialize the database
        mapObjectTypeRepository.saveAndFlush(mapObjectType);

        // Get the mapObjectType
        restMapObjectTypeMockMvc
            .perform(get(ENTITY_API_URL_ID, mapObjectType.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mapObjectType.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE));
    }

    @Test
    @Transactional
    void getNonExistingMapObjectType() throws Exception {
        // Get the mapObjectType
        restMapObjectTypeMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMapObjectType() throws Exception {
        // Initialize the database
        mapObjectTypeRepository.saveAndFlush(mapObjectType);

        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();

        // Update the mapObjectType
        MapObjectType updatedMapObjectType = mapObjectTypeRepository.findById(mapObjectType.getId()).get();
        // Disconnect from session so that the updates on updatedMapObjectType are not directly saved in db
        em.detach(updatedMapObjectType);
        updatedMapObjectType.title(UPDATED_TITLE);

        restMapObjectTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMapObjectType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMapObjectType))
            )
            .andExpect(status().isOk());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
        MapObjectType testMapObjectType = mapObjectTypeList.get(mapObjectTypeList.size() - 1);
        assertThat(testMapObjectType.getTitle()).isEqualTo(UPDATED_TITLE);
    }

    @Test
    @Transactional
    void putNonExistingMapObjectType() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();
        mapObjectType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMapObjectTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, mapObjectType.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mapObjectType))
            )
            .andExpect(status().isBadRequest());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMapObjectType() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();
        mapObjectType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMapObjectTypeMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mapObjectType))
            )
            .andExpect(status().isBadRequest());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMapObjectType() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();
        mapObjectType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMapObjectTypeMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObjectType)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMapObjectTypeWithPatch() throws Exception {
        // Initialize the database
        mapObjectTypeRepository.saveAndFlush(mapObjectType);

        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();

        // Update the mapObjectType using partial update
        MapObjectType partialUpdatedMapObjectType = new MapObjectType();
        partialUpdatedMapObjectType.setId(mapObjectType.getId());

        restMapObjectTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMapObjectType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMapObjectType))
            )
            .andExpect(status().isOk());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
        MapObjectType testMapObjectType = mapObjectTypeList.get(mapObjectTypeList.size() - 1);
        assertThat(testMapObjectType.getTitle()).isEqualTo(DEFAULT_TITLE);
    }

    @Test
    @Transactional
    void fullUpdateMapObjectTypeWithPatch() throws Exception {
        // Initialize the database
        mapObjectTypeRepository.saveAndFlush(mapObjectType);

        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();

        // Update the mapObjectType using partial update
        MapObjectType partialUpdatedMapObjectType = new MapObjectType();
        partialUpdatedMapObjectType.setId(mapObjectType.getId());

        partialUpdatedMapObjectType.title(UPDATED_TITLE);

        restMapObjectTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMapObjectType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMapObjectType))
            )
            .andExpect(status().isOk());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
        MapObjectType testMapObjectType = mapObjectTypeList.get(mapObjectTypeList.size() - 1);
        assertThat(testMapObjectType.getTitle()).isEqualTo(UPDATED_TITLE);
    }

    @Test
    @Transactional
    void patchNonExistingMapObjectType() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();
        mapObjectType.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMapObjectTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, mapObjectType.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mapObjectType))
            )
            .andExpect(status().isBadRequest());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMapObjectType() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();
        mapObjectType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMapObjectTypeMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mapObjectType))
            )
            .andExpect(status().isBadRequest());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMapObjectType() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectTypeRepository.findAll().size();
        mapObjectType.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMapObjectTypeMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(mapObjectType))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MapObjectType in the database
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMapObjectType() throws Exception {
        // Initialize the database
        mapObjectTypeRepository.saveAndFlush(mapObjectType);

        int databaseSizeBeforeDelete = mapObjectTypeRepository.findAll().size();

        // Delete the mapObjectType
        restMapObjectTypeMockMvc
            .perform(delete(ENTITY_API_URL_ID, mapObjectType.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MapObjectType> mapObjectTypeList = mapObjectTypeRepository.findAll();
        assertThat(mapObjectTypeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
