package io.route.admin.web.rest;

import static io.route.admin.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.route.admin.IntegrationTest;
import io.route.admin.domain.Route;
import io.route.admin.domain.enumeration.Complexity;
import io.route.admin.domain.enumeration.RouteCoverage;
import io.route.admin.domain.enumeration.RouteType;
import io.route.admin.repository.RouteRepository;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link RouteResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class RouteResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final RouteType DEFAULT_ROUTE_TYPE = RouteType.HIKING;
    private static final RouteType UPDATED_ROUTE_TYPE = RouteType.WATER;

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final Long DEFAULT_LENGTH = 1L;
    private static final Long UPDATED_LENGTH = 2L;

    private static final Boolean DEFAULT_LOOPED = false;
    private static final Boolean UPDATED_LOOPED = true;

    private static final Boolean DEFAULT_ACCESSIBLE = false;
    private static final Boolean UPDATED_ACCESSIBLE = true;

    private static final Complexity DEFAULT_COMPLEXITY = Complexity.EASY;
    private static final Complexity UPDATED_COMPLEXITY = Complexity.NORMAL;

    private static final String DEFAULT_PRICE = "AAAAAAAAAA";
    private static final String UPDATED_PRICE = "BBBBBBBBBB";

    private static final Integer DEFAULT_DAYS = 1;
    private static final Integer UPDATED_DAYS = 2;

    private static final Integer DEFAULT_HOURS = 1;
    private static final Integer UPDATED_HOURS = 2;

    private static final RouteCoverage DEFAULT_COVERAGE = RouteCoverage.ASPHALT;
    private static final RouteCoverage UPDATED_COVERAGE = RouteCoverage.GRAVEL;

    private static final ZonedDateTime DEFAULT_CREATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/routes";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private RouteRepository routeRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restRouteMockMvc;

    private Route route;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Route createEntity(EntityManager em) {
        Route route = new Route()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .routeType(DEFAULT_ROUTE_TYPE)
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE)
            .length(DEFAULT_LENGTH)
            .looped(DEFAULT_LOOPED)
            .accessible(DEFAULT_ACCESSIBLE)
            .complexity(DEFAULT_COMPLEXITY)
            .price(DEFAULT_PRICE)
            .days(DEFAULT_DAYS)
            .hours(DEFAULT_HOURS)
            .coverage(DEFAULT_COVERAGE)
            .create(DEFAULT_CREATE)
            .update(DEFAULT_UPDATE);
        return route;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static Route createUpdatedEntity(EntityManager em) {
        Route route = new Route()
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .routeType(UPDATED_ROUTE_TYPE)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .length(UPDATED_LENGTH)
            .looped(UPDATED_LOOPED)
            .accessible(UPDATED_ACCESSIBLE)
            .complexity(UPDATED_COMPLEXITY)
            .price(UPDATED_PRICE)
            .days(UPDATED_DAYS)
            .hours(UPDATED_HOURS)
            .coverage(UPDATED_COVERAGE)
            .create(UPDATED_CREATE)
            .update(UPDATED_UPDATE);
        return route;
    }

    @BeforeEach
    public void initTest() {
        route = createEntity(em);
    }

    @Test
    @Transactional
    void createRoute() throws Exception {
        int databaseSizeBeforeCreate = routeRepository.findAll().size();
        // Create the Route
        restRouteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(route)))
            .andExpect(status().isCreated());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeCreate + 1);
        Route testRoute = routeList.get(routeList.size() - 1);
        assertThat(testRoute.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testRoute.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testRoute.getRouteType()).isEqualTo(DEFAULT_ROUTE_TYPE);
        assertThat(testRoute.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testRoute.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testRoute.getLength()).isEqualTo(DEFAULT_LENGTH);
        assertThat(testRoute.getLooped()).isEqualTo(DEFAULT_LOOPED);
        assertThat(testRoute.getAccessible()).isEqualTo(DEFAULT_ACCESSIBLE);
        assertThat(testRoute.getComplexity()).isEqualTo(DEFAULT_COMPLEXITY);
        assertThat(testRoute.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testRoute.getDays()).isEqualTo(DEFAULT_DAYS);
        assertThat(testRoute.getHours()).isEqualTo(DEFAULT_HOURS);
        assertThat(testRoute.getCoverage()).isEqualTo(DEFAULT_COVERAGE);
        assertThat(testRoute.getCreate()).isEqualTo(DEFAULT_CREATE);
        assertThat(testRoute.getUpdate()).isEqualTo(DEFAULT_UPDATE);
    }

    @Test
    @Transactional
    void createRouteWithExistingId() throws Exception {
        // Create the Route with an existing ID
        route.setId(1L);

        int databaseSizeBeforeCreate = routeRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restRouteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(route)))
            .andExpect(status().isBadRequest());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = routeRepository.findAll().size();
        // set the field null
        route.setTitle(null);

        // Create the Route, which fails.

        restRouteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(route)))
            .andExpect(status().isBadRequest());

        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLengthIsRequired() throws Exception {
        int databaseSizeBeforeTest = routeRepository.findAll().size();
        // set the field null
        route.setLength(null);

        // Create the Route, which fails.

        restRouteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(route)))
            .andExpect(status().isBadRequest());

        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreateIsRequired() throws Exception {
        int databaseSizeBeforeTest = routeRepository.findAll().size();
        // set the field null
        route.setCreate(null);

        // Create the Route, which fails.

        restRouteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(route)))
            .andExpect(status().isBadRequest());

        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = routeRepository.findAll().size();
        // set the field null
        route.setUpdate(null);

        // Create the Route, which fails.

        restRouteMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(route)))
            .andExpect(status().isBadRequest());

        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllRoutes() throws Exception {
        // Initialize the database
        routeRepository.saveAndFlush(route);

        // Get all the routeList
        restRouteMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(route.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].routeType").value(hasItem(DEFAULT_ROUTE_TYPE.toString())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].length").value(hasItem(DEFAULT_LENGTH.intValue())))
            .andExpect(jsonPath("$.[*].looped").value(hasItem(DEFAULT_LOOPED.booleanValue())))
            .andExpect(jsonPath("$.[*].accessible").value(hasItem(DEFAULT_ACCESSIBLE.booleanValue())))
            .andExpect(jsonPath("$.[*].complexity").value(hasItem(DEFAULT_COMPLEXITY.toString())))
            .andExpect(jsonPath("$.[*].price").value(hasItem(DEFAULT_PRICE)))
            .andExpect(jsonPath("$.[*].days").value(hasItem(DEFAULT_DAYS)))
            .andExpect(jsonPath("$.[*].hours").value(hasItem(DEFAULT_HOURS)))
            .andExpect(jsonPath("$.[*].coverage").value(hasItem(DEFAULT_COVERAGE.toString())))
            .andExpect(jsonPath("$.[*].create").value(hasItem(sameInstant(DEFAULT_CREATE))))
            .andExpect(jsonPath("$.[*].update").value(hasItem(sameInstant(DEFAULT_UPDATE))));
    }

    @Test
    @Transactional
    void getRoute() throws Exception {
        // Initialize the database
        routeRepository.saveAndFlush(route);

        // Get the route
        restRouteMockMvc
            .perform(get(ENTITY_API_URL_ID, route.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(route.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.routeType").value(DEFAULT_ROUTE_TYPE.toString()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.length").value(DEFAULT_LENGTH.intValue()))
            .andExpect(jsonPath("$.looped").value(DEFAULT_LOOPED.booleanValue()))
            .andExpect(jsonPath("$.accessible").value(DEFAULT_ACCESSIBLE.booleanValue()))
            .andExpect(jsonPath("$.complexity").value(DEFAULT_COMPLEXITY.toString()))
            .andExpect(jsonPath("$.price").value(DEFAULT_PRICE))
            .andExpect(jsonPath("$.days").value(DEFAULT_DAYS))
            .andExpect(jsonPath("$.hours").value(DEFAULT_HOURS))
            .andExpect(jsonPath("$.coverage").value(DEFAULT_COVERAGE.toString()))
            .andExpect(jsonPath("$.create").value(sameInstant(DEFAULT_CREATE)))
            .andExpect(jsonPath("$.update").value(sameInstant(DEFAULT_UPDATE)));
    }

    @Test
    @Transactional
    void getNonExistingRoute() throws Exception {
        // Get the route
        restRouteMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewRoute() throws Exception {
        // Initialize the database
        routeRepository.saveAndFlush(route);

        int databaseSizeBeforeUpdate = routeRepository.findAll().size();

        // Update the route
        Route updatedRoute = routeRepository.findById(route.getId()).get();
        // Disconnect from session so that the updates on updatedRoute are not directly saved in db
        em.detach(updatedRoute);
        updatedRoute
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .routeType(UPDATED_ROUTE_TYPE)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .length(UPDATED_LENGTH)
            .looped(UPDATED_LOOPED)
            .accessible(UPDATED_ACCESSIBLE)
            .complexity(UPDATED_COMPLEXITY)
            .price(UPDATED_PRICE)
            .days(UPDATED_DAYS)
            .hours(UPDATED_HOURS)
            .coverage(UPDATED_COVERAGE)
            .create(UPDATED_CREATE)
            .update(UPDATED_UPDATE);

        restRouteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedRoute.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedRoute))
            )
            .andExpect(status().isOk());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
        Route testRoute = routeList.get(routeList.size() - 1);
        assertThat(testRoute.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testRoute.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRoute.getRouteType()).isEqualTo(UPDATED_ROUTE_TYPE);
        assertThat(testRoute.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testRoute.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testRoute.getLength()).isEqualTo(UPDATED_LENGTH);
        assertThat(testRoute.getLooped()).isEqualTo(UPDATED_LOOPED);
        assertThat(testRoute.getAccessible()).isEqualTo(UPDATED_ACCESSIBLE);
        assertThat(testRoute.getComplexity()).isEqualTo(UPDATED_COMPLEXITY);
        assertThat(testRoute.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testRoute.getDays()).isEqualTo(UPDATED_DAYS);
        assertThat(testRoute.getHours()).isEqualTo(UPDATED_HOURS);
        assertThat(testRoute.getCoverage()).isEqualTo(UPDATED_COVERAGE);
        assertThat(testRoute.getCreate()).isEqualTo(UPDATED_CREATE);
        assertThat(testRoute.getUpdate()).isEqualTo(UPDATED_UPDATE);
    }

    @Test
    @Transactional
    void putNonExistingRoute() throws Exception {
        int databaseSizeBeforeUpdate = routeRepository.findAll().size();
        route.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRouteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, route.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(route))
            )
            .andExpect(status().isBadRequest());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchRoute() throws Exception {
        int databaseSizeBeforeUpdate = routeRepository.findAll().size();
        route.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRouteMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(route))
            )
            .andExpect(status().isBadRequest());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamRoute() throws Exception {
        int databaseSizeBeforeUpdate = routeRepository.findAll().size();
        route.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRouteMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(route)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateRouteWithPatch() throws Exception {
        // Initialize the database
        routeRepository.saveAndFlush(route);

        int databaseSizeBeforeUpdate = routeRepository.findAll().size();

        // Update the route using partial update
        Route partialUpdatedRoute = new Route();
        partialUpdatedRoute.setId(route.getId());

        partialUpdatedRoute
            .description(UPDATED_DESCRIPTION)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .looped(UPDATED_LOOPED)
            .accessible(UPDATED_ACCESSIBLE)
            .hours(UPDATED_HOURS)
            .coverage(UPDATED_COVERAGE)
            .update(UPDATED_UPDATE);

        restRouteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRoute.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRoute))
            )
            .andExpect(status().isOk());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
        Route testRoute = routeList.get(routeList.size() - 1);
        assertThat(testRoute.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testRoute.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRoute.getRouteType()).isEqualTo(DEFAULT_ROUTE_TYPE);
        assertThat(testRoute.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testRoute.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testRoute.getLength()).isEqualTo(DEFAULT_LENGTH);
        assertThat(testRoute.getLooped()).isEqualTo(UPDATED_LOOPED);
        assertThat(testRoute.getAccessible()).isEqualTo(UPDATED_ACCESSIBLE);
        assertThat(testRoute.getComplexity()).isEqualTo(DEFAULT_COMPLEXITY);
        assertThat(testRoute.getPrice()).isEqualTo(DEFAULT_PRICE);
        assertThat(testRoute.getDays()).isEqualTo(DEFAULT_DAYS);
        assertThat(testRoute.getHours()).isEqualTo(UPDATED_HOURS);
        assertThat(testRoute.getCoverage()).isEqualTo(UPDATED_COVERAGE);
        assertThat(testRoute.getCreate()).isEqualTo(DEFAULT_CREATE);
        assertThat(testRoute.getUpdate()).isEqualTo(UPDATED_UPDATE);
    }

    @Test
    @Transactional
    void fullUpdateRouteWithPatch() throws Exception {
        // Initialize the database
        routeRepository.saveAndFlush(route);

        int databaseSizeBeforeUpdate = routeRepository.findAll().size();

        // Update the route using partial update
        Route partialUpdatedRoute = new Route();
        partialUpdatedRoute.setId(route.getId());

        partialUpdatedRoute
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .routeType(UPDATED_ROUTE_TYPE)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .length(UPDATED_LENGTH)
            .looped(UPDATED_LOOPED)
            .accessible(UPDATED_ACCESSIBLE)
            .complexity(UPDATED_COMPLEXITY)
            .price(UPDATED_PRICE)
            .days(UPDATED_DAYS)
            .hours(UPDATED_HOURS)
            .coverage(UPDATED_COVERAGE)
            .create(UPDATED_CREATE)
            .update(UPDATED_UPDATE);

        restRouteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedRoute.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedRoute))
            )
            .andExpect(status().isOk());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
        Route testRoute = routeList.get(routeList.size() - 1);
        assertThat(testRoute.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testRoute.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testRoute.getRouteType()).isEqualTo(UPDATED_ROUTE_TYPE);
        assertThat(testRoute.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testRoute.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testRoute.getLength()).isEqualTo(UPDATED_LENGTH);
        assertThat(testRoute.getLooped()).isEqualTo(UPDATED_LOOPED);
        assertThat(testRoute.getAccessible()).isEqualTo(UPDATED_ACCESSIBLE);
        assertThat(testRoute.getComplexity()).isEqualTo(UPDATED_COMPLEXITY);
        assertThat(testRoute.getPrice()).isEqualTo(UPDATED_PRICE);
        assertThat(testRoute.getDays()).isEqualTo(UPDATED_DAYS);
        assertThat(testRoute.getHours()).isEqualTo(UPDATED_HOURS);
        assertThat(testRoute.getCoverage()).isEqualTo(UPDATED_COVERAGE);
        assertThat(testRoute.getCreate()).isEqualTo(UPDATED_CREATE);
        assertThat(testRoute.getUpdate()).isEqualTo(UPDATED_UPDATE);
    }

    @Test
    @Transactional
    void patchNonExistingRoute() throws Exception {
        int databaseSizeBeforeUpdate = routeRepository.findAll().size();
        route.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restRouteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, route.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(route))
            )
            .andExpect(status().isBadRequest());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchRoute() throws Exception {
        int databaseSizeBeforeUpdate = routeRepository.findAll().size();
        route.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRouteMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(route))
            )
            .andExpect(status().isBadRequest());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamRoute() throws Exception {
        int databaseSizeBeforeUpdate = routeRepository.findAll().size();
        route.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restRouteMockMvc
            .perform(patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(route)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the Route in the database
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteRoute() throws Exception {
        // Initialize the database
        routeRepository.saveAndFlush(route);

        int databaseSizeBeforeDelete = routeRepository.findAll().size();

        // Delete the route
        restRouteMockMvc
            .perform(delete(ENTITY_API_URL_ID, route.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<Route> routeList = routeRepository.findAll();
        assertThat(routeList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
