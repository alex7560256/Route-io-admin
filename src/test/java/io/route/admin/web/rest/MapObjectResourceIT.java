package io.route.admin.web.rest;

import static io.route.admin.web.rest.TestUtil.sameInstant;
import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

import io.route.admin.IntegrationTest;
import io.route.admin.domain.MapObject;
import io.route.admin.repository.MapObjectRepository;
import java.time.Instant;
import java.time.LocalDate;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.time.ZonedDateTime;
import java.util.List;
import java.util.Random;
import java.util.concurrent.atomic.AtomicLong;
import javax.persistence.EntityManager;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Base64Utils;

/**
 * Integration tests for the {@link MapObjectResource} REST controller.
 */
@IntegrationTest
@AutoConfigureMockMvc
@WithMockUser
class MapObjectResourceIT {

    private static final String DEFAULT_TITLE = "AAAAAAAAAA";
    private static final String UPDATED_TITLE = "BBBBBBBBBB";

    private static final String DEFAULT_DESCRIPTION = "AAAAAAAAAA";
    private static final String UPDATED_DESCRIPTION = "BBBBBBBBBB";

    private static final Long DEFAULT_LAT = 1L;
    private static final Long UPDATED_LAT = 2L;

    private static final Long DEFAULT_LANG = 1L;
    private static final Long UPDATED_LANG = 2L;

    private static final byte[] DEFAULT_PHOTO = TestUtil.createByteArray(1, "0");
    private static final byte[] UPDATED_PHOTO = TestUtil.createByteArray(1, "1");
    private static final String DEFAULT_PHOTO_CONTENT_TYPE = "image/jpg";
    private static final String UPDATED_PHOTO_CONTENT_TYPE = "image/png";

    private static final LocalDate DEFAULT_CONSTRUCTIONS_DATE = LocalDate.ofEpochDay(0L);
    private static final LocalDate UPDATED_CONSTRUCTIONS_DATE = LocalDate.now(ZoneId.systemDefault());

    private static final Boolean DEFAULT_PAID = false;
    private static final Boolean UPDATED_PAID = true;

    private static final Long DEFAULT_COST = 1L;
    private static final Long UPDATED_COST = 2L;

    private static final Long DEFAULT_ALTITUDE = 1L;
    private static final Long UPDATED_ALTITUDE = 2L;

    private static final ZonedDateTime DEFAULT_CREATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_CREATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final ZonedDateTime DEFAULT_UPDATE = ZonedDateTime.ofInstant(Instant.ofEpochMilli(0L), ZoneOffset.UTC);
    private static final ZonedDateTime UPDATED_UPDATE = ZonedDateTime.now(ZoneId.systemDefault()).withNano(0);

    private static final String ENTITY_API_URL = "/api/map-objects";
    private static final String ENTITY_API_URL_ID = ENTITY_API_URL + "/{id}";

    private static Random random = new Random();
    private static AtomicLong count = new AtomicLong(random.nextInt() + (2 * Integer.MAX_VALUE));

    @Autowired
    private MapObjectRepository mapObjectRepository;

    @Autowired
    private EntityManager em;

    @Autowired
    private MockMvc restMapObjectMockMvc;

    private MapObject mapObject;

    /**
     * Create an entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MapObject createEntity(EntityManager em) {
        MapObject mapObject = new MapObject()
            .title(DEFAULT_TITLE)
            .description(DEFAULT_DESCRIPTION)
            .lat(DEFAULT_LAT)
            .lang(DEFAULT_LANG)
            .photo(DEFAULT_PHOTO)
            .photoContentType(DEFAULT_PHOTO_CONTENT_TYPE)
            .constructionsDate(DEFAULT_CONSTRUCTIONS_DATE)
            .paid(DEFAULT_PAID)
            .cost(DEFAULT_COST)
            .altitude(DEFAULT_ALTITUDE)
            .create(DEFAULT_CREATE)
            .update(DEFAULT_UPDATE);
        return mapObject;
    }

    /**
     * Create an updated entity for this test.
     *
     * This is a static method, as tests for other entities might also need it,
     * if they test an entity which requires the current entity.
     */
    public static MapObject createUpdatedEntity(EntityManager em) {
        MapObject mapObject = new MapObject()
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .lat(UPDATED_LAT)
            .lang(UPDATED_LANG)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .constructionsDate(UPDATED_CONSTRUCTIONS_DATE)
            .paid(UPDATED_PAID)
            .cost(UPDATED_COST)
            .altitude(UPDATED_ALTITUDE)
            .create(UPDATED_CREATE)
            .update(UPDATED_UPDATE);
        return mapObject;
    }

    @BeforeEach
    public void initTest() {
        mapObject = createEntity(em);
    }

    @Test
    @Transactional
    void createMapObject() throws Exception {
        int databaseSizeBeforeCreate = mapObjectRepository.findAll().size();
        // Create the MapObject
        restMapObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObject)))
            .andExpect(status().isCreated());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeCreate + 1);
        MapObject testMapObject = mapObjectList.get(mapObjectList.size() - 1);
        assertThat(testMapObject.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testMapObject.getDescription()).isEqualTo(DEFAULT_DESCRIPTION);
        assertThat(testMapObject.getLat()).isEqualTo(DEFAULT_LAT);
        assertThat(testMapObject.getLang()).isEqualTo(DEFAULT_LANG);
        assertThat(testMapObject.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testMapObject.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testMapObject.getConstructionsDate()).isEqualTo(DEFAULT_CONSTRUCTIONS_DATE);
        assertThat(testMapObject.getPaid()).isEqualTo(DEFAULT_PAID);
        assertThat(testMapObject.getCost()).isEqualTo(DEFAULT_COST);
        assertThat(testMapObject.getAltitude()).isEqualTo(DEFAULT_ALTITUDE);
        assertThat(testMapObject.getCreate()).isEqualTo(DEFAULT_CREATE);
        assertThat(testMapObject.getUpdate()).isEqualTo(DEFAULT_UPDATE);
    }

    @Test
    @Transactional
    void createMapObjectWithExistingId() throws Exception {
        // Create the MapObject with an existing ID
        mapObject.setId(1L);

        int databaseSizeBeforeCreate = mapObjectRepository.findAll().size();

        // An entity with an existing ID cannot be created, so this API call must fail
        restMapObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObject)))
            .andExpect(status().isBadRequest());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeCreate);
    }

    @Test
    @Transactional
    void checkTitleIsRequired() throws Exception {
        int databaseSizeBeforeTest = mapObjectRepository.findAll().size();
        // set the field null
        mapObject.setTitle(null);

        // Create the MapObject, which fails.

        restMapObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObject)))
            .andExpect(status().isBadRequest());

        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLatIsRequired() throws Exception {
        int databaseSizeBeforeTest = mapObjectRepository.findAll().size();
        // set the field null
        mapObject.setLat(null);

        // Create the MapObject, which fails.

        restMapObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObject)))
            .andExpect(status().isBadRequest());

        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkLangIsRequired() throws Exception {
        int databaseSizeBeforeTest = mapObjectRepository.findAll().size();
        // set the field null
        mapObject.setLang(null);

        // Create the MapObject, which fails.

        restMapObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObject)))
            .andExpect(status().isBadRequest());

        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkCreateIsRequired() throws Exception {
        int databaseSizeBeforeTest = mapObjectRepository.findAll().size();
        // set the field null
        mapObject.setCreate(null);

        // Create the MapObject, which fails.

        restMapObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObject)))
            .andExpect(status().isBadRequest());

        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void checkUpdateIsRequired() throws Exception {
        int databaseSizeBeforeTest = mapObjectRepository.findAll().size();
        // set the field null
        mapObject.setUpdate(null);

        // Create the MapObject, which fails.

        restMapObjectMockMvc
            .perform(post(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObject)))
            .andExpect(status().isBadRequest());

        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeTest);
    }

    @Test
    @Transactional
    void getAllMapObjects() throws Exception {
        // Initialize the database
        mapObjectRepository.saveAndFlush(mapObject);

        // Get all the mapObjectList
        restMapObjectMockMvc
            .perform(get(ENTITY_API_URL + "?sort=id,desc"))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.[*].id").value(hasItem(mapObject.getId().intValue())))
            .andExpect(jsonPath("$.[*].title").value(hasItem(DEFAULT_TITLE)))
            .andExpect(jsonPath("$.[*].description").value(hasItem(DEFAULT_DESCRIPTION)))
            .andExpect(jsonPath("$.[*].lat").value(hasItem(DEFAULT_LAT.intValue())))
            .andExpect(jsonPath("$.[*].lang").value(hasItem(DEFAULT_LANG.intValue())))
            .andExpect(jsonPath("$.[*].photoContentType").value(hasItem(DEFAULT_PHOTO_CONTENT_TYPE)))
            .andExpect(jsonPath("$.[*].photo").value(hasItem(Base64Utils.encodeToString(DEFAULT_PHOTO))))
            .andExpect(jsonPath("$.[*].constructionsDate").value(hasItem(DEFAULT_CONSTRUCTIONS_DATE.toString())))
            .andExpect(jsonPath("$.[*].paid").value(hasItem(DEFAULT_PAID.booleanValue())))
            .andExpect(jsonPath("$.[*].cost").value(hasItem(DEFAULT_COST.intValue())))
            .andExpect(jsonPath("$.[*].altitude").value(hasItem(DEFAULT_ALTITUDE.intValue())))
            .andExpect(jsonPath("$.[*].create").value(hasItem(sameInstant(DEFAULT_CREATE))))
            .andExpect(jsonPath("$.[*].update").value(hasItem(sameInstant(DEFAULT_UPDATE))));
    }

    @Test
    @Transactional
    void getMapObject() throws Exception {
        // Initialize the database
        mapObjectRepository.saveAndFlush(mapObject);

        // Get the mapObject
        restMapObjectMockMvc
            .perform(get(ENTITY_API_URL_ID, mapObject.getId()))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaType.APPLICATION_JSON_VALUE))
            .andExpect(jsonPath("$.id").value(mapObject.getId().intValue()))
            .andExpect(jsonPath("$.title").value(DEFAULT_TITLE))
            .andExpect(jsonPath("$.description").value(DEFAULT_DESCRIPTION))
            .andExpect(jsonPath("$.lat").value(DEFAULT_LAT.intValue()))
            .andExpect(jsonPath("$.lang").value(DEFAULT_LANG.intValue()))
            .andExpect(jsonPath("$.photoContentType").value(DEFAULT_PHOTO_CONTENT_TYPE))
            .andExpect(jsonPath("$.photo").value(Base64Utils.encodeToString(DEFAULT_PHOTO)))
            .andExpect(jsonPath("$.constructionsDate").value(DEFAULT_CONSTRUCTIONS_DATE.toString()))
            .andExpect(jsonPath("$.paid").value(DEFAULT_PAID.booleanValue()))
            .andExpect(jsonPath("$.cost").value(DEFAULT_COST.intValue()))
            .andExpect(jsonPath("$.altitude").value(DEFAULT_ALTITUDE.intValue()))
            .andExpect(jsonPath("$.create").value(sameInstant(DEFAULT_CREATE)))
            .andExpect(jsonPath("$.update").value(sameInstant(DEFAULT_UPDATE)));
    }

    @Test
    @Transactional
    void getNonExistingMapObject() throws Exception {
        // Get the mapObject
        restMapObjectMockMvc.perform(get(ENTITY_API_URL_ID, Long.MAX_VALUE)).andExpect(status().isNotFound());
    }

    @Test
    @Transactional
    void putNewMapObject() throws Exception {
        // Initialize the database
        mapObjectRepository.saveAndFlush(mapObject);

        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();

        // Update the mapObject
        MapObject updatedMapObject = mapObjectRepository.findById(mapObject.getId()).get();
        // Disconnect from session so that the updates on updatedMapObject are not directly saved in db
        em.detach(updatedMapObject);
        updatedMapObject
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .lat(UPDATED_LAT)
            .lang(UPDATED_LANG)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .constructionsDate(UPDATED_CONSTRUCTIONS_DATE)
            .paid(UPDATED_PAID)
            .cost(UPDATED_COST)
            .altitude(UPDATED_ALTITUDE)
            .create(UPDATED_CREATE)
            .update(UPDATED_UPDATE);

        restMapObjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, updatedMapObject.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(updatedMapObject))
            )
            .andExpect(status().isOk());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
        MapObject testMapObject = mapObjectList.get(mapObjectList.size() - 1);
        assertThat(testMapObject.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testMapObject.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testMapObject.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testMapObject.getLang()).isEqualTo(UPDATED_LANG);
        assertThat(testMapObject.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testMapObject.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testMapObject.getConstructionsDate()).isEqualTo(UPDATED_CONSTRUCTIONS_DATE);
        assertThat(testMapObject.getPaid()).isEqualTo(UPDATED_PAID);
        assertThat(testMapObject.getCost()).isEqualTo(UPDATED_COST);
        assertThat(testMapObject.getAltitude()).isEqualTo(UPDATED_ALTITUDE);
        assertThat(testMapObject.getCreate()).isEqualTo(UPDATED_CREATE);
        assertThat(testMapObject.getUpdate()).isEqualTo(UPDATED_UPDATE);
    }

    @Test
    @Transactional
    void putNonExistingMapObject() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();
        mapObject.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMapObjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, mapObject.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mapObject))
            )
            .andExpect(status().isBadRequest());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithIdMismatchMapObject() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();
        mapObject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMapObjectMockMvc
            .perform(
                put(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(TestUtil.convertObjectToJsonBytes(mapObject))
            )
            .andExpect(status().isBadRequest());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void putWithMissingIdPathParamMapObject() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();
        mapObject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMapObjectMockMvc
            .perform(put(ENTITY_API_URL).contentType(MediaType.APPLICATION_JSON).content(TestUtil.convertObjectToJsonBytes(mapObject)))
            .andExpect(status().isMethodNotAllowed());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void partialUpdateMapObjectWithPatch() throws Exception {
        // Initialize the database
        mapObjectRepository.saveAndFlush(mapObject);

        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();

        // Update the mapObject using partial update
        MapObject partialUpdatedMapObject = new MapObject();
        partialUpdatedMapObject.setId(mapObject.getId());

        partialUpdatedMapObject
            .description(UPDATED_DESCRIPTION)
            .lat(UPDATED_LAT)
            .lang(UPDATED_LANG)
            .constructionsDate(UPDATED_CONSTRUCTIONS_DATE)
            .paid(UPDATED_PAID)
            .cost(UPDATED_COST);

        restMapObjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMapObject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMapObject))
            )
            .andExpect(status().isOk());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
        MapObject testMapObject = mapObjectList.get(mapObjectList.size() - 1);
        assertThat(testMapObject.getTitle()).isEqualTo(DEFAULT_TITLE);
        assertThat(testMapObject.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testMapObject.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testMapObject.getLang()).isEqualTo(UPDATED_LANG);
        assertThat(testMapObject.getPhoto()).isEqualTo(DEFAULT_PHOTO);
        assertThat(testMapObject.getPhotoContentType()).isEqualTo(DEFAULT_PHOTO_CONTENT_TYPE);
        assertThat(testMapObject.getConstructionsDate()).isEqualTo(UPDATED_CONSTRUCTIONS_DATE);
        assertThat(testMapObject.getPaid()).isEqualTo(UPDATED_PAID);
        assertThat(testMapObject.getCost()).isEqualTo(UPDATED_COST);
        assertThat(testMapObject.getAltitude()).isEqualTo(DEFAULT_ALTITUDE);
        assertThat(testMapObject.getCreate()).isEqualTo(DEFAULT_CREATE);
        assertThat(testMapObject.getUpdate()).isEqualTo(DEFAULT_UPDATE);
    }

    @Test
    @Transactional
    void fullUpdateMapObjectWithPatch() throws Exception {
        // Initialize the database
        mapObjectRepository.saveAndFlush(mapObject);

        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();

        // Update the mapObject using partial update
        MapObject partialUpdatedMapObject = new MapObject();
        partialUpdatedMapObject.setId(mapObject.getId());

        partialUpdatedMapObject
            .title(UPDATED_TITLE)
            .description(UPDATED_DESCRIPTION)
            .lat(UPDATED_LAT)
            .lang(UPDATED_LANG)
            .photo(UPDATED_PHOTO)
            .photoContentType(UPDATED_PHOTO_CONTENT_TYPE)
            .constructionsDate(UPDATED_CONSTRUCTIONS_DATE)
            .paid(UPDATED_PAID)
            .cost(UPDATED_COST)
            .altitude(UPDATED_ALTITUDE)
            .create(UPDATED_CREATE)
            .update(UPDATED_UPDATE);

        restMapObjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, partialUpdatedMapObject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(partialUpdatedMapObject))
            )
            .andExpect(status().isOk());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
        MapObject testMapObject = mapObjectList.get(mapObjectList.size() - 1);
        assertThat(testMapObject.getTitle()).isEqualTo(UPDATED_TITLE);
        assertThat(testMapObject.getDescription()).isEqualTo(UPDATED_DESCRIPTION);
        assertThat(testMapObject.getLat()).isEqualTo(UPDATED_LAT);
        assertThat(testMapObject.getLang()).isEqualTo(UPDATED_LANG);
        assertThat(testMapObject.getPhoto()).isEqualTo(UPDATED_PHOTO);
        assertThat(testMapObject.getPhotoContentType()).isEqualTo(UPDATED_PHOTO_CONTENT_TYPE);
        assertThat(testMapObject.getConstructionsDate()).isEqualTo(UPDATED_CONSTRUCTIONS_DATE);
        assertThat(testMapObject.getPaid()).isEqualTo(UPDATED_PAID);
        assertThat(testMapObject.getCost()).isEqualTo(UPDATED_COST);
        assertThat(testMapObject.getAltitude()).isEqualTo(UPDATED_ALTITUDE);
        assertThat(testMapObject.getCreate()).isEqualTo(UPDATED_CREATE);
        assertThat(testMapObject.getUpdate()).isEqualTo(UPDATED_UPDATE);
    }

    @Test
    @Transactional
    void patchNonExistingMapObject() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();
        mapObject.setId(count.incrementAndGet());

        // If the entity doesn't have an ID, it will throw BadRequestAlertException
        restMapObjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, mapObject.getId())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mapObject))
            )
            .andExpect(status().isBadRequest());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithIdMismatchMapObject() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();
        mapObject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMapObjectMockMvc
            .perform(
                patch(ENTITY_API_URL_ID, count.incrementAndGet())
                    .contentType("application/merge-patch+json")
                    .content(TestUtil.convertObjectToJsonBytes(mapObject))
            )
            .andExpect(status().isBadRequest());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void patchWithMissingIdPathParamMapObject() throws Exception {
        int databaseSizeBeforeUpdate = mapObjectRepository.findAll().size();
        mapObject.setId(count.incrementAndGet());

        // If url ID doesn't match entity ID, it will throw BadRequestAlertException
        restMapObjectMockMvc
            .perform(
                patch(ENTITY_API_URL).contentType("application/merge-patch+json").content(TestUtil.convertObjectToJsonBytes(mapObject))
            )
            .andExpect(status().isMethodNotAllowed());

        // Validate the MapObject in the database
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeUpdate);
    }

    @Test
    @Transactional
    void deleteMapObject() throws Exception {
        // Initialize the database
        mapObjectRepository.saveAndFlush(mapObject);

        int databaseSizeBeforeDelete = mapObjectRepository.findAll().size();

        // Delete the mapObject
        restMapObjectMockMvc
            .perform(delete(ENTITY_API_URL_ID, mapObject.getId()).accept(MediaType.APPLICATION_JSON))
            .andExpect(status().isNoContent());

        // Validate the database contains one less item
        List<MapObject> mapObjectList = mapObjectRepository.findAll();
        assertThat(mapObjectList).hasSize(databaseSizeBeforeDelete - 1);
    }
}
