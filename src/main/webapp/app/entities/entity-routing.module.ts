import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: 'map-object',
        data: { pageTitle: 'routeApp.mapObject.home.title' },
        loadChildren: () => import('./map-object/map-object.module').then(m => m.MapObjectModule),
      },
      {
        path: 'route',
        data: { pageTitle: 'routeApp.route.home.title' },
        loadChildren: () => import('./route/route.module').then(m => m.RouteModule),
      },
      {
        path: 'map-object-type',
        data: { pageTitle: 'routeApp.mapObjectType.home.title' },
        loadChildren: () => import('./map-object-type/map-object-type.module').then(m => m.MapObjectTypeModule),
      },
      {
        path: 'tag',
        data: { pageTitle: 'routeApp.tag.home.title' },
        loadChildren: () => import('./tag/tag.module').then(m => m.TagModule),
      },
      {
        path: 'state',
        data: { pageTitle: 'routeApp.state.home.title' },
        loadChildren: () => import('./state/state.module').then(m => m.StateModule),
      },
      {
        path: 'region',
        data: { pageTitle: 'routeApp.region.home.title' },
        loadChildren: () => import('./region/region.module').then(m => m.RegionModule),
      },
      {
        path: 'country',
        data: { pageTitle: 'routeApp.country.home.title' },
        loadChildren: () => import('./country/country.module').then(m => m.CountryModule),
      },
      {
        path: 'point',
        data: { pageTitle: 'routeApp.point.home.title' },
        loadChildren: () => import('./point/point.module').then(m => m.PointModule),
      },
      {
        path: 'parent-object',
        data: { pageTitle: 'routeApp.parentObject.home.title' },
        loadChildren: () => import('./parent-object/parent-object.module').then(m => m.ParentObjectModule),
      },
      /* jhipster-needle-add-entity-route - JHipster will add entity modules routes here */
    ]),
  ],
})
export class EntityRoutingModule {}
