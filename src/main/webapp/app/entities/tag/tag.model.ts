import { IRoute } from 'app/entities/route/route.model';
import { IMapObject } from 'app/entities/map-object/map-object.model';

export interface ITag {
  id?: number;
  title?: string;
  route?: IRoute | null;
  mapObject?: IMapObject | null;
}

export class Tag implements ITag {
  constructor(public id?: number, public title?: string, public route?: IRoute | null, public mapObject?: IMapObject | null) {}
}

export function getTagIdentifier(tag: ITag): number | undefined {
  return tag.id;
}
