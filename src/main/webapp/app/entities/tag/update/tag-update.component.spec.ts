import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { TagService } from '../service/tag.service';
import { ITag, Tag } from '../tag.model';
import { IRoute } from 'app/entities/route/route.model';
import { RouteService } from 'app/entities/route/service/route.service';
import { IMapObject } from 'app/entities/map-object/map-object.model';
import { MapObjectService } from 'app/entities/map-object/service/map-object.service';

import { TagUpdateComponent } from './tag-update.component';

describe('Tag Management Update Component', () => {
  let comp: TagUpdateComponent;
  let fixture: ComponentFixture<TagUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let tagService: TagService;
  let routeService: RouteService;
  let mapObjectService: MapObjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [TagUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(TagUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(TagUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    tagService = TestBed.inject(TagService);
    routeService = TestBed.inject(RouteService);
    mapObjectService = TestBed.inject(MapObjectService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Route query and add missing value', () => {
      const tag: ITag = { id: 456 };
      const route: IRoute = { id: 8867 };
      tag.route = route;

      const routeCollection: IRoute[] = [{ id: 82530 }];
      jest.spyOn(routeService, 'query').mockReturnValue(of(new HttpResponse({ body: routeCollection })));
      const additionalRoutes = [route];
      const expectedCollection: IRoute[] = [...additionalRoutes, ...routeCollection];
      jest.spyOn(routeService, 'addRouteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ tag });
      comp.ngOnInit();

      expect(routeService.query).toHaveBeenCalled();
      expect(routeService.addRouteToCollectionIfMissing).toHaveBeenCalledWith(routeCollection, ...additionalRoutes);
      expect(comp.routesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call MapObject query and add missing value', () => {
      const tag: ITag = { id: 456 };
      const mapObject: IMapObject = { id: 95636 };
      tag.mapObject = mapObject;

      const mapObjectCollection: IMapObject[] = [{ id: 74067 }];
      jest.spyOn(mapObjectService, 'query').mockReturnValue(of(new HttpResponse({ body: mapObjectCollection })));
      const additionalMapObjects = [mapObject];
      const expectedCollection: IMapObject[] = [...additionalMapObjects, ...mapObjectCollection];
      jest.spyOn(mapObjectService, 'addMapObjectToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ tag });
      comp.ngOnInit();

      expect(mapObjectService.query).toHaveBeenCalled();
      expect(mapObjectService.addMapObjectToCollectionIfMissing).toHaveBeenCalledWith(mapObjectCollection, ...additionalMapObjects);
      expect(comp.mapObjectsSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const tag: ITag = { id: 456 };
      const route: IRoute = { id: 93830 };
      tag.route = route;
      const mapObject: IMapObject = { id: 40023 };
      tag.mapObject = mapObject;

      activatedRoute.data = of({ tag });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(tag));
      expect(comp.routesSharedCollection).toContain(route);
      expect(comp.mapObjectsSharedCollection).toContain(mapObject);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Tag>>();
      const tag = { id: 123 };
      jest.spyOn(tagService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tag });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: tag }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(tagService.update).toHaveBeenCalledWith(tag);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Tag>>();
      const tag = new Tag();
      jest.spyOn(tagService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tag });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: tag }));
      saveSubject.complete();

      // THEN
      expect(tagService.create).toHaveBeenCalledWith(tag);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Tag>>();
      const tag = { id: 123 };
      jest.spyOn(tagService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ tag });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(tagService.update).toHaveBeenCalledWith(tag);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackRouteById', () => {
      it('Should return tracked Route primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRouteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackMapObjectById', () => {
      it('Should return tracked MapObject primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMapObjectById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
