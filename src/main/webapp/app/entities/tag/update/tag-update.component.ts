import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { ITag, Tag } from '../tag.model';
import { TagService } from '../service/tag.service';
import { IRoute } from 'app/entities/route/route.model';
import { RouteService } from 'app/entities/route/service/route.service';
import { IMapObject } from 'app/entities/map-object/map-object.model';
import { MapObjectService } from 'app/entities/map-object/service/map-object.service';

@Component({
  selector: 'jhi-tag-update',
  templateUrl: './tag-update.component.html',
})
export class TagUpdateComponent implements OnInit {
  isSaving = false;

  routesSharedCollection: IRoute[] = [];
  mapObjectsSharedCollection: IMapObject[] = [];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
    route: [],
    mapObject: [],
  });

  constructor(
    protected tagService: TagService,
    protected routeService: RouteService,
    protected mapObjectService: MapObjectService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ tag }) => {
      this.updateForm(tag);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const tag = this.createFromForm();
    if (tag.id !== undefined) {
      this.subscribeToSaveResponse(this.tagService.update(tag));
    } else {
      this.subscribeToSaveResponse(this.tagService.create(tag));
    }
  }

  trackRouteById(index: number, item: IRoute): number {
    return item.id!;
  }

  trackMapObjectById(index: number, item: IMapObject): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<ITag>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(tag: ITag): void {
    this.editForm.patchValue({
      id: tag.id,
      title: tag.title,
      route: tag.route,
      mapObject: tag.mapObject,
    });

    this.routesSharedCollection = this.routeService.addRouteToCollectionIfMissing(this.routesSharedCollection, tag.route);
    this.mapObjectsSharedCollection = this.mapObjectService.addMapObjectToCollectionIfMissing(
      this.mapObjectsSharedCollection,
      tag.mapObject
    );
  }

  protected loadRelationshipsOptions(): void {
    this.routeService
      .query()
      .pipe(map((res: HttpResponse<IRoute[]>) => res.body ?? []))
      .pipe(map((routes: IRoute[]) => this.routeService.addRouteToCollectionIfMissing(routes, this.editForm.get('route')!.value)))
      .subscribe((routes: IRoute[]) => (this.routesSharedCollection = routes));

    this.mapObjectService
      .query()
      .pipe(map((res: HttpResponse<IMapObject[]>) => res.body ?? []))
      .pipe(
        map((mapObjects: IMapObject[]) =>
          this.mapObjectService.addMapObjectToCollectionIfMissing(mapObjects, this.editForm.get('mapObject')!.value)
        )
      )
      .subscribe((mapObjects: IMapObject[]) => (this.mapObjectsSharedCollection = mapObjects));
  }

  protected createFromForm(): ITag {
    return {
      ...new Tag(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      route: this.editForm.get(['route'])!.value,
      mapObject: this.editForm.get(['mapObject'])!.value,
    };
  }
}
