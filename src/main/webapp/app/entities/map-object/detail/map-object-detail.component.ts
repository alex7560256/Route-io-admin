import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMapObject } from '../map-object.model';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-map-object-detail',
  templateUrl: './map-object-detail.component.html',
})
export class MapObjectDetailComponent implements OnInit {
  mapObject: IMapObject | null = null;

  constructor(protected dataUtils: DataUtils, protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mapObject }) => {
      this.mapObject = mapObject;
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  previousState(): void {
    window.history.back();
  }
}
