import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMapObject, MapObject } from '../map-object.model';
import { MapObjectService } from '../service/map-object.service';

@Injectable({ providedIn: 'root' })
export class MapObjectRoutingResolveService implements Resolve<IMapObject> {
  constructor(protected service: MapObjectService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMapObject> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((mapObject: HttpResponse<MapObject>) => {
          if (mapObject.body) {
            return of(mapObject.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MapObject());
  }
}
