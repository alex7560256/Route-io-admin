import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MapObjectComponent } from '../list/map-object.component';
import { MapObjectDetailComponent } from '../detail/map-object-detail.component';
import { MapObjectUpdateComponent } from '../update/map-object-update.component';
import { MapObjectRoutingResolveService } from './map-object-routing-resolve.service';

const mapObjectRoute: Routes = [
  {
    path: '',
    component: MapObjectComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MapObjectDetailComponent,
    resolve: {
      mapObject: MapObjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MapObjectUpdateComponent,
    resolve: {
      mapObject: MapObjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MapObjectUpdateComponent,
    resolve: {
      mapObject: MapObjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(mapObjectRoute)],
  exports: [RouterModule],
})
export class MapObjectRoutingModule {}
