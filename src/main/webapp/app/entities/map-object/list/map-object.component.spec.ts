import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { MapObjectService } from '../service/map-object.service';

import { MapObjectComponent } from './map-object.component';

describe('MapObject Management Component', () => {
  let comp: MapObjectComponent;
  let fixture: ComponentFixture<MapObjectComponent>;
  let service: MapObjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MapObjectComponent],
    })
      .overrideTemplate(MapObjectComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MapObjectComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(MapObjectService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.mapObjects?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
