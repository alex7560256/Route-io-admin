import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMapObject } from '../map-object.model';
import { MapObjectService } from '../service/map-object.service';
import { MapObjectDeleteDialogComponent } from '../delete/map-object-delete-dialog.component';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-map-object',
  templateUrl: './map-object.component.html',
})
export class MapObjectComponent implements OnInit {
  mapObjects?: IMapObject[];
  isLoading = false;

  constructor(protected mapObjectService: MapObjectService, protected dataUtils: DataUtils, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.mapObjectService.query().subscribe({
      next: (res: HttpResponse<IMapObject[]>) => {
        this.isLoading = false;
        this.mapObjects = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMapObject): number {
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    return this.dataUtils.openFile(base64String, contentType);
  }

  delete(mapObject: IMapObject): void {
    const modalRef = this.modalService.open(MapObjectDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.mapObject = mapObject;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
