import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMapObject } from '../map-object.model';
import { MapObjectService } from '../service/map-object.service';

@Component({
  templateUrl: './map-object-delete-dialog.component.html',
})
export class MapObjectDeleteDialogComponent {
  mapObject?: IMapObject;

  constructor(protected mapObjectService: MapObjectService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.mapObjectService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
