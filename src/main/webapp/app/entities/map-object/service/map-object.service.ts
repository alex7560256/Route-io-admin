import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { DATE_FORMAT } from 'app/config/input.constants';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMapObject, getMapObjectIdentifier } from '../map-object.model';

export type EntityResponseType = HttpResponse<IMapObject>;
export type EntityArrayResponseType = HttpResponse<IMapObject[]>;

@Injectable({ providedIn: 'root' })
export class MapObjectService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/map-objects');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(mapObject: IMapObject): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mapObject);
    return this.http
      .post<IMapObject>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(mapObject: IMapObject): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mapObject);
    return this.http
      .put<IMapObject>(`${this.resourceUrl}/${getMapObjectIdentifier(mapObject) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(mapObject: IMapObject): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(mapObject);
    return this.http
      .patch<IMapObject>(`${this.resourceUrl}/${getMapObjectIdentifier(mapObject) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IMapObject>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IMapObject[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMapObjectToCollectionIfMissing(
    mapObjectCollection: IMapObject[],
    ...mapObjectsToCheck: (IMapObject | null | undefined)[]
  ): IMapObject[] {
    const mapObjects: IMapObject[] = mapObjectsToCheck.filter(isPresent);
    if (mapObjects.length > 0) {
      const mapObjectCollectionIdentifiers = mapObjectCollection.map(mapObjectItem => getMapObjectIdentifier(mapObjectItem)!);
      const mapObjectsToAdd = mapObjects.filter(mapObjectItem => {
        const mapObjectIdentifier = getMapObjectIdentifier(mapObjectItem);
        if (mapObjectIdentifier == null || mapObjectCollectionIdentifiers.includes(mapObjectIdentifier)) {
          return false;
        }
        mapObjectCollectionIdentifiers.push(mapObjectIdentifier);
        return true;
      });
      return [...mapObjectsToAdd, ...mapObjectCollection];
    }
    return mapObjectCollection;
  }

  protected convertDateFromClient(mapObject: IMapObject): IMapObject {
    return Object.assign({}, mapObject, {
      constructionsDate: mapObject.constructionsDate?.isValid() ? mapObject.constructionsDate.format(DATE_FORMAT) : undefined,
      create: mapObject.create?.isValid() ? mapObject.create.toJSON() : undefined,
      update: mapObject.update?.isValid() ? mapObject.update.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.constructionsDate = res.body.constructionsDate ? dayjs(res.body.constructionsDate) : undefined;
      res.body.create = res.body.create ? dayjs(res.body.create) : undefined;
      res.body.update = res.body.update ? dayjs(res.body.update) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((mapObject: IMapObject) => {
        mapObject.constructionsDate = mapObject.constructionsDate ? dayjs(mapObject.constructionsDate) : undefined;
        mapObject.create = mapObject.create ? dayjs(mapObject.create) : undefined;
        mapObject.update = mapObject.update ? dayjs(mapObject.update) : undefined;
      });
    }
    return res;
  }
}
