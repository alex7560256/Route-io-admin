import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import dayjs from 'dayjs/esm';

import { DATE_FORMAT, DATE_TIME_FORMAT } from 'app/config/input.constants';
import { IMapObject, MapObject } from '../map-object.model';

import { MapObjectService } from './map-object.service';

describe('MapObject Service', () => {
  let service: MapObjectService;
  let httpMock: HttpTestingController;
  let elemDefault: IMapObject;
  let expectedResult: IMapObject | IMapObject[] | boolean | null;
  let currentDate: dayjs.Dayjs;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MapObjectService);
    httpMock = TestBed.inject(HttpTestingController);
    currentDate = dayjs();

    elemDefault = {
      id: 0,
      title: 'AAAAAAA',
      description: 'AAAAAAA',
      lat: 0,
      lang: 0,
      photoContentType: 'image/png',
      photo: 'AAAAAAA',
      constructionsDate: currentDate,
      paid: false,
      cost: 0,
      altitude: 0,
      create: currentDate,
      update: currentDate,
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign(
        {
          constructionsDate: currentDate.format(DATE_FORMAT),
          create: currentDate.format(DATE_TIME_FORMAT),
          update: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a MapObject', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
          constructionsDate: currentDate.format(DATE_FORMAT),
          create: currentDate.format(DATE_TIME_FORMAT),
          update: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          constructionsDate: currentDate,
          create: currentDate,
          update: currentDate,
        },
        returnedFromService
      );

      service.create(new MapObject()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a MapObject', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          title: 'BBBBBB',
          description: 'BBBBBB',
          lat: 1,
          lang: 1,
          photo: 'BBBBBB',
          constructionsDate: currentDate.format(DATE_FORMAT),
          paid: true,
          cost: 1,
          altitude: 1,
          create: currentDate.format(DATE_TIME_FORMAT),
          update: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          constructionsDate: currentDate,
          create: currentDate,
          update: currentDate,
        },
        returnedFromService
      );

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a MapObject', () => {
      const patchObject = Object.assign(
        {
          title: 'BBBBBB',
          constructionsDate: currentDate.format(DATE_FORMAT),
          paid: true,
          cost: 1,
          create: currentDate.format(DATE_TIME_FORMAT),
        },
        new MapObject()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign(
        {
          constructionsDate: currentDate,
          create: currentDate,
          update: currentDate,
        },
        returnedFromService
      );

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of MapObject', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          title: 'BBBBBB',
          description: 'BBBBBB',
          lat: 1,
          lang: 1,
          photo: 'BBBBBB',
          constructionsDate: currentDate.format(DATE_FORMAT),
          paid: true,
          cost: 1,
          altitude: 1,
          create: currentDate.format(DATE_TIME_FORMAT),
          update: currentDate.format(DATE_TIME_FORMAT),
        },
        elemDefault
      );

      const expected = Object.assign(
        {
          constructionsDate: currentDate,
          create: currentDate,
          update: currentDate,
        },
        returnedFromService
      );

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a MapObject', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMapObjectToCollectionIfMissing', () => {
      it('should add a MapObject to an empty array', () => {
        const mapObject: IMapObject = { id: 123 };
        expectedResult = service.addMapObjectToCollectionIfMissing([], mapObject);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(mapObject);
      });

      it('should not add a MapObject to an array that contains it', () => {
        const mapObject: IMapObject = { id: 123 };
        const mapObjectCollection: IMapObject[] = [
          {
            ...mapObject,
          },
          { id: 456 },
        ];
        expectedResult = service.addMapObjectToCollectionIfMissing(mapObjectCollection, mapObject);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a MapObject to an array that doesn't contain it", () => {
        const mapObject: IMapObject = { id: 123 };
        const mapObjectCollection: IMapObject[] = [{ id: 456 }];
        expectedResult = service.addMapObjectToCollectionIfMissing(mapObjectCollection, mapObject);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(mapObject);
      });

      it('should add only unique MapObject to an array', () => {
        const mapObjectArray: IMapObject[] = [{ id: 123 }, { id: 456 }, { id: 81598 }];
        const mapObjectCollection: IMapObject[] = [{ id: 123 }];
        expectedResult = service.addMapObjectToCollectionIfMissing(mapObjectCollection, ...mapObjectArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const mapObject: IMapObject = { id: 123 };
        const mapObject2: IMapObject = { id: 456 };
        expectedResult = service.addMapObjectToCollectionIfMissing([], mapObject, mapObject2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(mapObject);
        expect(expectedResult).toContain(mapObject2);
      });

      it('should accept null and undefined values', () => {
        const mapObject: IMapObject = { id: 123 };
        expectedResult = service.addMapObjectToCollectionIfMissing([], null, mapObject, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(mapObject);
      });

      it('should return initial array if no MapObject is added', () => {
        const mapObjectCollection: IMapObject[] = [{ id: 123 }];
        expectedResult = service.addMapObjectToCollectionIfMissing(mapObjectCollection, undefined, null);
        expect(expectedResult).toEqual(mapObjectCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
