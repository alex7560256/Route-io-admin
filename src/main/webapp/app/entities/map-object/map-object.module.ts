import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MapObjectComponent } from './list/map-object.component';
import { MapObjectDetailComponent } from './detail/map-object-detail.component';
import { MapObjectUpdateComponent } from './update/map-object-update.component';
import { MapObjectDeleteDialogComponent } from './delete/map-object-delete-dialog.component';
import { MapObjectRoutingModule } from './route/map-object-routing.module';

@NgModule({
  imports: [SharedModule, MapObjectRoutingModule],
  declarations: [MapObjectComponent, MapObjectDetailComponent, MapObjectUpdateComponent, MapObjectDeleteDialogComponent],
  entryComponents: [MapObjectDeleteDialogComponent],
})
export class MapObjectModule {}
