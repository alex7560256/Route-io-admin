import dayjs from 'dayjs/esm';
import { ITag } from 'app/entities/tag/tag.model';
import { ICountry } from 'app/entities/country/country.model';
import { IRegion } from 'app/entities/region/region.model';
import { IState } from 'app/entities/state/state.model';
import { IMapObjectType } from 'app/entities/map-object-type/map-object-type.model';
import { IRoute } from 'app/entities/route/route.model';

export interface IMapObject {
  id?: number;
  title?: string;
  description?: string | null;
  lat?: number;
  lang?: number;
  photoContentType?: string | null;
  photo?: string | null;
  constructionsDate?: dayjs.Dayjs | null;
  paid?: boolean | null;
  cost?: number | null;
  altitude?: number | null;
  create?: dayjs.Dayjs;
  update?: dayjs.Dayjs;
  tags?: ITag[] | null;
  country?: ICountry | null;
  region?: IRegion | null;
  state?: IState | null;
  type?: IMapObjectType | null;
  route?: IRoute | null;
}

export class MapObject implements IMapObject {
  constructor(
    public id?: number,
    public title?: string,
    public description?: string | null,
    public lat?: number,
    public lang?: number,
    public photoContentType?: string | null,
    public photo?: string | null,
    public constructionsDate?: dayjs.Dayjs | null,
    public paid?: boolean | null,
    public cost?: number | null,
    public altitude?: number | null,
    public create?: dayjs.Dayjs,
    public update?: dayjs.Dayjs,
    public tags?: ITag[] | null,
    public country?: ICountry | null,
    public region?: IRegion | null,
    public state?: IState | null,
    public type?: IMapObjectType | null,
    public route?: IRoute | null
  ) {
    this.paid = this.paid ?? false;
  }
}

export function getMapObjectIdentifier(mapObject: IMapObject): number | undefined {
  return mapObject.id;
}
