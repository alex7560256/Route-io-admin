import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IMapObject, MapObject } from '../map-object.model';
import { MapObjectService } from '../service/map-object.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { IRegion } from 'app/entities/region/region.model';
import { RegionService } from 'app/entities/region/service/region.service';
import { IState } from 'app/entities/state/state.model';
import { StateService } from 'app/entities/state/service/state.service';
import { IMapObjectType } from 'app/entities/map-object-type/map-object-type.model';
import { MapObjectTypeService } from 'app/entities/map-object-type/service/map-object-type.service';
import { IRoute } from 'app/entities/route/route.model';
import { RouteService } from 'app/entities/route/service/route.service';

@Component({
  selector: 'jhi-map-object-update',
  templateUrl: './map-object-update.component.html',
})
export class MapObjectUpdateComponent implements OnInit {
  isSaving = false;

  countriesSharedCollection: ICountry[] = [];
  regionsSharedCollection: IRegion[] = [];
  statesSharedCollection: IState[] = [];
  mapObjectTypesSharedCollection: IMapObjectType[] = [];
  routesSharedCollection: IRoute[] = [];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
    description: [],
    lat: [null, [Validators.required]],
    lang: [null, [Validators.required]],
    photo: [],
    photoContentType: [],
    constructionsDate: [],
    paid: [],
    cost: [],
    altitude: [],
    create: [null, [Validators.required]],
    update: [null, [Validators.required]],
    country: [],
    region: [],
    state: [],
    type: [],
    route: [],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected mapObjectService: MapObjectService,
    protected countryService: CountryService,
    protected regionService: RegionService,
    protected stateService: StateService,
    protected mapObjectTypeService: MapObjectTypeService,
    protected routeService: RouteService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mapObject }) => {
      if (mapObject.id === undefined) {
        const today = dayjs().startOf('day');
        mapObject.create = today;
        mapObject.update = today;
      }

      this.updateForm(mapObject);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(new EventWithContent<AlertError>('routeApp.error', { ...err, key: 'error.file.' + err.key })),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const mapObject = this.createFromForm();
    if (mapObject.id !== undefined) {
      this.subscribeToSaveResponse(this.mapObjectService.update(mapObject));
    } else {
      this.subscribeToSaveResponse(this.mapObjectService.create(mapObject));
    }
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackRegionById(index: number, item: IRegion): number {
    return item.id!;
  }

  trackStateById(index: number, item: IState): number {
    return item.id!;
  }

  trackMapObjectTypeById(index: number, item: IMapObjectType): number {
    return item.id!;
  }

  trackRouteById(index: number, item: IRoute): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMapObject>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(mapObject: IMapObject): void {
    this.editForm.patchValue({
      id: mapObject.id,
      title: mapObject.title,
      description: mapObject.description,
      lat: mapObject.lat,
      lang: mapObject.lang,
      photo: mapObject.photo,
      photoContentType: mapObject.photoContentType,
      constructionsDate: mapObject.constructionsDate,
      paid: mapObject.paid,
      cost: mapObject.cost,
      altitude: mapObject.altitude,
      create: mapObject.create ? mapObject.create.format(DATE_TIME_FORMAT) : null,
      update: mapObject.update ? mapObject.update.format(DATE_TIME_FORMAT) : null,
      country: mapObject.country,
      region: mapObject.region,
      state: mapObject.state,
      type: mapObject.type,
      route: mapObject.route,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(this.countriesSharedCollection, mapObject.country);
    this.regionsSharedCollection = this.regionService.addRegionToCollectionIfMissing(this.regionsSharedCollection, mapObject.region);
    this.statesSharedCollection = this.stateService.addStateToCollectionIfMissing(this.statesSharedCollection, mapObject.state);
    this.mapObjectTypesSharedCollection = this.mapObjectTypeService.addMapObjectTypeToCollectionIfMissing(
      this.mapObjectTypesSharedCollection,
      mapObject.type
    );
    this.routesSharedCollection = this.routeService.addRouteToCollectionIfMissing(this.routesSharedCollection, mapObject.route);
  }

  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.regionService
      .query()
      .pipe(map((res: HttpResponse<IRegion[]>) => res.body ?? []))
      .pipe(map((regions: IRegion[]) => this.regionService.addRegionToCollectionIfMissing(regions, this.editForm.get('region')!.value)))
      .subscribe((regions: IRegion[]) => (this.regionsSharedCollection = regions));

    this.stateService
      .query()
      .pipe(map((res: HttpResponse<IState[]>) => res.body ?? []))
      .pipe(map((states: IState[]) => this.stateService.addStateToCollectionIfMissing(states, this.editForm.get('state')!.value)))
      .subscribe((states: IState[]) => (this.statesSharedCollection = states));

    this.mapObjectTypeService
      .query()
      .pipe(map((res: HttpResponse<IMapObjectType[]>) => res.body ?? []))
      .pipe(
        map((mapObjectTypes: IMapObjectType[]) =>
          this.mapObjectTypeService.addMapObjectTypeToCollectionIfMissing(mapObjectTypes, this.editForm.get('type')!.value)
        )
      )
      .subscribe((mapObjectTypes: IMapObjectType[]) => (this.mapObjectTypesSharedCollection = mapObjectTypes));

    this.routeService
      .query()
      .pipe(map((res: HttpResponse<IRoute[]>) => res.body ?? []))
      .pipe(map((routes: IRoute[]) => this.routeService.addRouteToCollectionIfMissing(routes, this.editForm.get('route')!.value)))
      .subscribe((routes: IRoute[]) => (this.routesSharedCollection = routes));
  }

  protected createFromForm(): IMapObject {
    return {
      ...new MapObject(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      description: this.editForm.get(['description'])!.value,
      lat: this.editForm.get(['lat'])!.value,
      lang: this.editForm.get(['lang'])!.value,
      photoContentType: this.editForm.get(['photoContentType'])!.value,
      photo: this.editForm.get(['photo'])!.value,
      constructionsDate: this.editForm.get(['constructionsDate'])!.value,
      paid: this.editForm.get(['paid'])!.value,
      cost: this.editForm.get(['cost'])!.value,
      altitude: this.editForm.get(['altitude'])!.value,
      create: this.editForm.get(['create'])!.value ? dayjs(this.editForm.get(['create'])!.value, DATE_TIME_FORMAT) : undefined,
      update: this.editForm.get(['update'])!.value ? dayjs(this.editForm.get(['update'])!.value, DATE_TIME_FORMAT) : undefined,
      country: this.editForm.get(['country'])!.value,
      region: this.editForm.get(['region'])!.value,
      state: this.editForm.get(['state'])!.value,
      type: this.editForm.get(['type'])!.value,
      route: this.editForm.get(['route'])!.value,
    };
  }
}
