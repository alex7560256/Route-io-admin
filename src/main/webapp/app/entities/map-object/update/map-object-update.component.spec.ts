import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { MapObjectService } from '../service/map-object.service';
import { IMapObject, MapObject } from '../map-object.model';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { IRegion } from 'app/entities/region/region.model';
import { RegionService } from 'app/entities/region/service/region.service';
import { IState } from 'app/entities/state/state.model';
import { StateService } from 'app/entities/state/service/state.service';
import { IMapObjectType } from 'app/entities/map-object-type/map-object-type.model';
import { MapObjectTypeService } from 'app/entities/map-object-type/service/map-object-type.service';
import { IRoute } from 'app/entities/route/route.model';
import { RouteService } from 'app/entities/route/service/route.service';

import { MapObjectUpdateComponent } from './map-object-update.component';

describe('MapObject Management Update Component', () => {
  let comp: MapObjectUpdateComponent;
  let fixture: ComponentFixture<MapObjectUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let mapObjectService: MapObjectService;
  let countryService: CountryService;
  let regionService: RegionService;
  let stateService: StateService;
  let mapObjectTypeService: MapObjectTypeService;
  let routeService: RouteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [MapObjectUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(MapObjectUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MapObjectUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    mapObjectService = TestBed.inject(MapObjectService);
    countryService = TestBed.inject(CountryService);
    regionService = TestBed.inject(RegionService);
    stateService = TestBed.inject(StateService);
    mapObjectTypeService = TestBed.inject(MapObjectTypeService);
    routeService = TestBed.inject(RouteService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Country query and add missing value', () => {
      const mapObject: IMapObject = { id: 456 };
      const country: ICountry = { id: 96139 };
      mapObject.country = country;

      const countryCollection: ICountry[] = [{ id: 51990 }];
      jest.spyOn(countryService, 'query').mockReturnValue(of(new HttpResponse({ body: countryCollection })));
      const additionalCountries = [country];
      const expectedCollection: ICountry[] = [...additionalCountries, ...countryCollection];
      jest.spyOn(countryService, 'addCountryToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      expect(countryService.query).toHaveBeenCalled();
      expect(countryService.addCountryToCollectionIfMissing).toHaveBeenCalledWith(countryCollection, ...additionalCountries);
      expect(comp.countriesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Region query and add missing value', () => {
      const mapObject: IMapObject = { id: 456 };
      const region: IRegion = { id: 18774 };
      mapObject.region = region;

      const regionCollection: IRegion[] = [{ id: 90535 }];
      jest.spyOn(regionService, 'query').mockReturnValue(of(new HttpResponse({ body: regionCollection })));
      const additionalRegions = [region];
      const expectedCollection: IRegion[] = [...additionalRegions, ...regionCollection];
      jest.spyOn(regionService, 'addRegionToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      expect(regionService.query).toHaveBeenCalled();
      expect(regionService.addRegionToCollectionIfMissing).toHaveBeenCalledWith(regionCollection, ...additionalRegions);
      expect(comp.regionsSharedCollection).toEqual(expectedCollection);
    });

    it('Should call State query and add missing value', () => {
      const mapObject: IMapObject = { id: 456 };
      const state: IState = { id: 9628 };
      mapObject.state = state;

      const stateCollection: IState[] = [{ id: 89274 }];
      jest.spyOn(stateService, 'query').mockReturnValue(of(new HttpResponse({ body: stateCollection })));
      const additionalStates = [state];
      const expectedCollection: IState[] = [...additionalStates, ...stateCollection];
      jest.spyOn(stateService, 'addStateToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      expect(stateService.query).toHaveBeenCalled();
      expect(stateService.addStateToCollectionIfMissing).toHaveBeenCalledWith(stateCollection, ...additionalStates);
      expect(comp.statesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call MapObjectType query and add missing value', () => {
      const mapObject: IMapObject = { id: 456 };
      const type: IMapObjectType = { id: 5767 };
      mapObject.type = type;

      const mapObjectTypeCollection: IMapObjectType[] = [{ id: 83480 }];
      jest.spyOn(mapObjectTypeService, 'query').mockReturnValue(of(new HttpResponse({ body: mapObjectTypeCollection })));
      const additionalMapObjectTypes = [type];
      const expectedCollection: IMapObjectType[] = [...additionalMapObjectTypes, ...mapObjectTypeCollection];
      jest.spyOn(mapObjectTypeService, 'addMapObjectTypeToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      expect(mapObjectTypeService.query).toHaveBeenCalled();
      expect(mapObjectTypeService.addMapObjectTypeToCollectionIfMissing).toHaveBeenCalledWith(
        mapObjectTypeCollection,
        ...additionalMapObjectTypes
      );
      expect(comp.mapObjectTypesSharedCollection).toEqual(expectedCollection);
    });

    it('Should call Route query and add missing value', () => {
      const mapObject: IMapObject = { id: 456 };
      const route: IRoute = { id: 39474 };
      mapObject.route = route;

      const routeCollection: IRoute[] = [{ id: 74363 }];
      jest.spyOn(routeService, 'query').mockReturnValue(of(new HttpResponse({ body: routeCollection })));
      const additionalRoutes = [route];
      const expectedCollection: IRoute[] = [...additionalRoutes, ...routeCollection];
      jest.spyOn(routeService, 'addRouteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      expect(routeService.query).toHaveBeenCalled();
      expect(routeService.addRouteToCollectionIfMissing).toHaveBeenCalledWith(routeCollection, ...additionalRoutes);
      expect(comp.routesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const mapObject: IMapObject = { id: 456 };
      const country: ICountry = { id: 57257 };
      mapObject.country = country;
      const region: IRegion = { id: 13958 };
      mapObject.region = region;
      const state: IState = { id: 67265 };
      mapObject.state = state;
      const type: IMapObjectType = { id: 41351 };
      mapObject.type = type;
      const route: IRoute = { id: 8322 };
      mapObject.route = route;

      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(mapObject));
      expect(comp.countriesSharedCollection).toContain(country);
      expect(comp.regionsSharedCollection).toContain(region);
      expect(comp.statesSharedCollection).toContain(state);
      expect(comp.mapObjectTypesSharedCollection).toContain(type);
      expect(comp.routesSharedCollection).toContain(route);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MapObject>>();
      const mapObject = { id: 123 };
      jest.spyOn(mapObjectService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: mapObject }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(mapObjectService.update).toHaveBeenCalledWith(mapObject);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MapObject>>();
      const mapObject = new MapObject();
      jest.spyOn(mapObjectService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: mapObject }));
      saveSubject.complete();

      // THEN
      expect(mapObjectService.create).toHaveBeenCalledWith(mapObject);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MapObject>>();
      const mapObject = { id: 123 };
      jest.spyOn(mapObjectService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mapObject });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(mapObjectService.update).toHaveBeenCalledWith(mapObject);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackCountryById', () => {
      it('Should return tracked Country primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackCountryById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackRegionById', () => {
      it('Should return tracked Region primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRegionById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackStateById', () => {
      it('Should return tracked State primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackStateById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackMapObjectTypeById', () => {
      it('Should return tracked MapObjectType primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackMapObjectTypeById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });

    describe('trackRouteById', () => {
      it('Should return tracked Route primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRouteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
