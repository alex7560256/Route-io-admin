import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IMapObjectType, MapObjectType } from '../map-object-type.model';
import { MapObjectTypeService } from '../service/map-object-type.service';

@Injectable({ providedIn: 'root' })
export class MapObjectTypeRoutingResolveService implements Resolve<IMapObjectType> {
  constructor(protected service: MapObjectTypeService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IMapObjectType> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((mapObjectType: HttpResponse<MapObjectType>) => {
          if (mapObjectType.body) {
            return of(mapObjectType.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new MapObjectType());
  }
}
