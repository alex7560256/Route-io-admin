import { TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { ActivatedRouteSnapshot, ActivatedRoute, Router, convertToParamMap } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';

import { IMapObjectType, MapObjectType } from '../map-object-type.model';
import { MapObjectTypeService } from '../service/map-object-type.service';

import { MapObjectTypeRoutingResolveService } from './map-object-type-routing-resolve.service';

describe('MapObjectType routing resolve service', () => {
  let mockRouter: Router;
  let mockActivatedRouteSnapshot: ActivatedRouteSnapshot;
  let routingResolveService: MapObjectTypeRoutingResolveService;
  let service: MapObjectTypeService;
  let resultMapObjectType: IMapObjectType | undefined;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: {
            snapshot: {
              paramMap: convertToParamMap({}),
            },
          },
        },
      ],
    });
    mockRouter = TestBed.inject(Router);
    jest.spyOn(mockRouter, 'navigate').mockImplementation(() => Promise.resolve(true));
    mockActivatedRouteSnapshot = TestBed.inject(ActivatedRoute).snapshot;
    routingResolveService = TestBed.inject(MapObjectTypeRoutingResolveService);
    service = TestBed.inject(MapObjectTypeService);
    resultMapObjectType = undefined;
  });

  describe('resolve', () => {
    it('should return IMapObjectType returned by find', () => {
      // GIVEN
      service.find = jest.fn(id => of(new HttpResponse({ body: { id } })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMapObjectType = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMapObjectType).toEqual({ id: 123 });
    });

    it('should return new IMapObjectType if id is not provided', () => {
      // GIVEN
      service.find = jest.fn();
      mockActivatedRouteSnapshot.params = {};

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMapObjectType = result;
      });

      // THEN
      expect(service.find).not.toBeCalled();
      expect(resultMapObjectType).toEqual(new MapObjectType());
    });

    it('should route to 404 page if data not found in server', () => {
      // GIVEN
      jest.spyOn(service, 'find').mockReturnValue(of(new HttpResponse({ body: null as unknown as MapObjectType })));
      mockActivatedRouteSnapshot.params = { id: 123 };

      // WHEN
      routingResolveService.resolve(mockActivatedRouteSnapshot).subscribe(result => {
        resultMapObjectType = result;
      });

      // THEN
      expect(service.find).toBeCalledWith(123);
      expect(resultMapObjectType).toEqual(undefined);
      expect(mockRouter.navigate).toHaveBeenCalledWith(['404']);
    });
  });
});
