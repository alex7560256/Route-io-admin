import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { MapObjectTypeComponent } from '../list/map-object-type.component';
import { MapObjectTypeDetailComponent } from '../detail/map-object-type-detail.component';
import { MapObjectTypeUpdateComponent } from '../update/map-object-type-update.component';
import { MapObjectTypeRoutingResolveService } from './map-object-type-routing-resolve.service';

const mapObjectTypeRoute: Routes = [
  {
    path: '',
    component: MapObjectTypeComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: MapObjectTypeDetailComponent,
    resolve: {
      mapObjectType: MapObjectTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: MapObjectTypeUpdateComponent,
    resolve: {
      mapObjectType: MapObjectTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: MapObjectTypeUpdateComponent,
    resolve: {
      mapObjectType: MapObjectTypeRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(mapObjectTypeRoute)],
  exports: [RouterModule],
})
export class MapObjectTypeRoutingModule {}
