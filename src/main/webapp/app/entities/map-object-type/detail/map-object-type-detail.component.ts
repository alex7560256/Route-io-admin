import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IMapObjectType } from '../map-object-type.model';

@Component({
  selector: 'jhi-map-object-type-detail',
  templateUrl: './map-object-type-detail.component.html',
})
export class MapObjectTypeDetailComponent implements OnInit {
  mapObjectType: IMapObjectType | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mapObjectType }) => {
      this.mapObjectType = mapObjectType;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
