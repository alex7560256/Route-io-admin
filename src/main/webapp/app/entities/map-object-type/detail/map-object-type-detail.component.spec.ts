import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { MapObjectTypeDetailComponent } from './map-object-type-detail.component';

describe('MapObjectType Management Detail Component', () => {
  let comp: MapObjectTypeDetailComponent;
  let fixture: ComponentFixture<MapObjectTypeDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [MapObjectTypeDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ mapObjectType: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(MapObjectTypeDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(MapObjectTypeDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load mapObjectType on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.mapObjectType).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
