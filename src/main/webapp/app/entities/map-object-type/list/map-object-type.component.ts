import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IMapObjectType } from '../map-object-type.model';
import { MapObjectTypeService } from '../service/map-object-type.service';
import { MapObjectTypeDeleteDialogComponent } from '../delete/map-object-type-delete-dialog.component';

@Component({
  selector: 'jhi-map-object-type',
  templateUrl: './map-object-type.component.html',
})
export class MapObjectTypeComponent implements OnInit {
  mapObjectTypes?: IMapObjectType[];
  isLoading = false;

  constructor(protected mapObjectTypeService: MapObjectTypeService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.mapObjectTypeService.query().subscribe({
      next: (res: HttpResponse<IMapObjectType[]>) => {
        this.isLoading = false;
        this.mapObjectTypes = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IMapObjectType): number {
    return item.id!;
  }

  delete(mapObjectType: IMapObjectType): void {
    const modalRef = this.modalService.open(MapObjectTypeDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.mapObjectType = mapObjectType;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
