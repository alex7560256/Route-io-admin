import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { MapObjectTypeService } from '../service/map-object-type.service';

import { MapObjectTypeComponent } from './map-object-type.component';

describe('MapObjectType Management Component', () => {
  let comp: MapObjectTypeComponent;
  let fixture: ComponentFixture<MapObjectTypeComponent>;
  let service: MapObjectTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [MapObjectTypeComponent],
    })
      .overrideTemplate(MapObjectTypeComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MapObjectTypeComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(MapObjectTypeService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.mapObjectTypes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
