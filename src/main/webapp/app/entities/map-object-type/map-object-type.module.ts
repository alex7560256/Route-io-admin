import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { MapObjectTypeComponent } from './list/map-object-type.component';
import { MapObjectTypeDetailComponent } from './detail/map-object-type-detail.component';
import { MapObjectTypeUpdateComponent } from './update/map-object-type-update.component';
import { MapObjectTypeDeleteDialogComponent } from './delete/map-object-type-delete-dialog.component';
import { MapObjectTypeRoutingModule } from './route/map-object-type-routing.module';

@NgModule({
  imports: [SharedModule, MapObjectTypeRoutingModule],
  declarations: [MapObjectTypeComponent, MapObjectTypeDetailComponent, MapObjectTypeUpdateComponent, MapObjectTypeDeleteDialogComponent],
  entryComponents: [MapObjectTypeDeleteDialogComponent],
})
export class MapObjectTypeModule {}
