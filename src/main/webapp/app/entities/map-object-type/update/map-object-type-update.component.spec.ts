import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { MapObjectTypeService } from '../service/map-object-type.service';
import { IMapObjectType, MapObjectType } from '../map-object-type.model';

import { MapObjectTypeUpdateComponent } from './map-object-type-update.component';

describe('MapObjectType Management Update Component', () => {
  let comp: MapObjectTypeUpdateComponent;
  let fixture: ComponentFixture<MapObjectTypeUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let mapObjectTypeService: MapObjectTypeService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [MapObjectTypeUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(MapObjectTypeUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(MapObjectTypeUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    mapObjectTypeService = TestBed.inject(MapObjectTypeService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should update editForm', () => {
      const mapObjectType: IMapObjectType = { id: 456 };

      activatedRoute.data = of({ mapObjectType });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(mapObjectType));
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MapObjectType>>();
      const mapObjectType = { id: 123 };
      jest.spyOn(mapObjectTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mapObjectType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: mapObjectType }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(mapObjectTypeService.update).toHaveBeenCalledWith(mapObjectType);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MapObjectType>>();
      const mapObjectType = new MapObjectType();
      jest.spyOn(mapObjectTypeService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mapObjectType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: mapObjectType }));
      saveSubject.complete();

      // THEN
      expect(mapObjectTypeService.create).toHaveBeenCalledWith(mapObjectType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<MapObjectType>>();
      const mapObjectType = { id: 123 };
      jest.spyOn(mapObjectTypeService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ mapObjectType });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(mapObjectTypeService.update).toHaveBeenCalledWith(mapObjectType);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });
});
