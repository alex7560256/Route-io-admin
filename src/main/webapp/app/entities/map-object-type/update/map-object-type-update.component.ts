import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';

import { IMapObjectType, MapObjectType } from '../map-object-type.model';
import { MapObjectTypeService } from '../service/map-object-type.service';

@Component({
  selector: 'jhi-map-object-type-update',
  templateUrl: './map-object-type-update.component.html',
})
export class MapObjectTypeUpdateComponent implements OnInit {
  isSaving = false;

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
  });

  constructor(protected mapObjectTypeService: MapObjectTypeService, protected activatedRoute: ActivatedRoute, protected fb: FormBuilder) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ mapObjectType }) => {
      this.updateForm(mapObjectType);
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const mapObjectType = this.createFromForm();
    if (mapObjectType.id !== undefined) {
      this.subscribeToSaveResponse(this.mapObjectTypeService.update(mapObjectType));
    } else {
      this.subscribeToSaveResponse(this.mapObjectTypeService.create(mapObjectType));
    }
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IMapObjectType>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(mapObjectType: IMapObjectType): void {
    this.editForm.patchValue({
      id: mapObjectType.id,
      title: mapObjectType.title,
    });
  }

  protected createFromForm(): IMapObjectType {
    return {
      ...new MapObjectType(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
    };
  }
}
