import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IMapObjectType } from '../map-object-type.model';
import { MapObjectTypeService } from '../service/map-object-type.service';

@Component({
  templateUrl: './map-object-type-delete-dialog.component.html',
})
export class MapObjectTypeDeleteDialogComponent {
  mapObjectType?: IMapObjectType;

  constructor(protected mapObjectTypeService: MapObjectTypeService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.mapObjectTypeService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
