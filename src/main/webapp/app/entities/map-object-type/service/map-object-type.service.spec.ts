import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IMapObjectType, MapObjectType } from '../map-object-type.model';

import { MapObjectTypeService } from './map-object-type.service';

describe('MapObjectType Service', () => {
  let service: MapObjectTypeService;
  let httpMock: HttpTestingController;
  let elemDefault: IMapObjectType;
  let expectedResult: IMapObjectType | IMapObjectType[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(MapObjectTypeService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      title: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a MapObjectType', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new MapObjectType()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a MapObjectType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          title: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a MapObjectType', () => {
      const patchObject = Object.assign(
        {
          title: 'BBBBBB',
        },
        new MapObjectType()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of MapObjectType', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          title: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a MapObjectType', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addMapObjectTypeToCollectionIfMissing', () => {
      it('should add a MapObjectType to an empty array', () => {
        const mapObjectType: IMapObjectType = { id: 123 };
        expectedResult = service.addMapObjectTypeToCollectionIfMissing([], mapObjectType);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(mapObjectType);
      });

      it('should not add a MapObjectType to an array that contains it', () => {
        const mapObjectType: IMapObjectType = { id: 123 };
        const mapObjectTypeCollection: IMapObjectType[] = [
          {
            ...mapObjectType,
          },
          { id: 456 },
        ];
        expectedResult = service.addMapObjectTypeToCollectionIfMissing(mapObjectTypeCollection, mapObjectType);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a MapObjectType to an array that doesn't contain it", () => {
        const mapObjectType: IMapObjectType = { id: 123 };
        const mapObjectTypeCollection: IMapObjectType[] = [{ id: 456 }];
        expectedResult = service.addMapObjectTypeToCollectionIfMissing(mapObjectTypeCollection, mapObjectType);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(mapObjectType);
      });

      it('should add only unique MapObjectType to an array', () => {
        const mapObjectTypeArray: IMapObjectType[] = [{ id: 123 }, { id: 456 }, { id: 57376 }];
        const mapObjectTypeCollection: IMapObjectType[] = [{ id: 123 }];
        expectedResult = service.addMapObjectTypeToCollectionIfMissing(mapObjectTypeCollection, ...mapObjectTypeArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const mapObjectType: IMapObjectType = { id: 123 };
        const mapObjectType2: IMapObjectType = { id: 456 };
        expectedResult = service.addMapObjectTypeToCollectionIfMissing([], mapObjectType, mapObjectType2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(mapObjectType);
        expect(expectedResult).toContain(mapObjectType2);
      });

      it('should accept null and undefined values', () => {
        const mapObjectType: IMapObjectType = { id: 123 };
        expectedResult = service.addMapObjectTypeToCollectionIfMissing([], null, mapObjectType, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(mapObjectType);
      });

      it('should return initial array if no MapObjectType is added', () => {
        const mapObjectTypeCollection: IMapObjectType[] = [{ id: 123 }];
        expectedResult = service.addMapObjectTypeToCollectionIfMissing(mapObjectTypeCollection, undefined, null);
        expect(expectedResult).toEqual(mapObjectTypeCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
