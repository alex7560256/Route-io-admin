import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IMapObjectType, getMapObjectTypeIdentifier } from '../map-object-type.model';

export type EntityResponseType = HttpResponse<IMapObjectType>;
export type EntityArrayResponseType = HttpResponse<IMapObjectType[]>;

@Injectable({ providedIn: 'root' })
export class MapObjectTypeService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/map-object-types');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(mapObjectType: IMapObjectType): Observable<EntityResponseType> {
    return this.http.post<IMapObjectType>(this.resourceUrl, mapObjectType, { observe: 'response' });
  }

  update(mapObjectType: IMapObjectType): Observable<EntityResponseType> {
    return this.http.put<IMapObjectType>(`${this.resourceUrl}/${getMapObjectTypeIdentifier(mapObjectType) as number}`, mapObjectType, {
      observe: 'response',
    });
  }

  partialUpdate(mapObjectType: IMapObjectType): Observable<EntityResponseType> {
    return this.http.patch<IMapObjectType>(`${this.resourceUrl}/${getMapObjectTypeIdentifier(mapObjectType) as number}`, mapObjectType, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IMapObjectType>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IMapObjectType[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addMapObjectTypeToCollectionIfMissing(
    mapObjectTypeCollection: IMapObjectType[],
    ...mapObjectTypesToCheck: (IMapObjectType | null | undefined)[]
  ): IMapObjectType[] {
    const mapObjectTypes: IMapObjectType[] = mapObjectTypesToCheck.filter(isPresent);
    if (mapObjectTypes.length > 0) {
      const mapObjectTypeCollectionIdentifiers = mapObjectTypeCollection.map(
        mapObjectTypeItem => getMapObjectTypeIdentifier(mapObjectTypeItem)!
      );
      const mapObjectTypesToAdd = mapObjectTypes.filter(mapObjectTypeItem => {
        const mapObjectTypeIdentifier = getMapObjectTypeIdentifier(mapObjectTypeItem);
        if (mapObjectTypeIdentifier == null || mapObjectTypeCollectionIdentifiers.includes(mapObjectTypeIdentifier)) {
          return false;
        }
        mapObjectTypeCollectionIdentifiers.push(mapObjectTypeIdentifier);
        return true;
      });
      return [...mapObjectTypesToAdd, ...mapObjectTypeCollection];
    }
    return mapObjectTypeCollection;
  }
}
