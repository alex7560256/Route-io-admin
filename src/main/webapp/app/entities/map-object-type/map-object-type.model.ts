export interface IMapObjectType {
  id?: number;
  title?: string;
}

export class MapObjectType implements IMapObjectType {
  constructor(public id?: number, public title?: string) {}
}

export function getMapObjectTypeIdentifier(mapObjectType: IMapObjectType): number | undefined {
  return mapObjectType.id;
}
