export interface ICountry {
  id?: number;
  title?: string;
}

export class Country implements ICountry {
  constructor(public id?: number, public title?: string) {}
}

export function getCountryIdentifier(country: ICountry): number | undefined {
  return country.id;
}
