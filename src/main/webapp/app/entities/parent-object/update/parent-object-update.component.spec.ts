import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { ParentObjectService } from '../service/parent-object.service';
import { IParentObject, ParentObject } from '../parent-object.model';
import { IRoute } from 'app/entities/route/route.model';
import { RouteService } from 'app/entities/route/service/route.service';

import { ParentObjectUpdateComponent } from './parent-object-update.component';

describe('ParentObject Management Update Component', () => {
  let comp: ParentObjectUpdateComponent;
  let fixture: ComponentFixture<ParentObjectUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let parentObjectService: ParentObjectService;
  let routeService: RouteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [ParentObjectUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(ParentObjectUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ParentObjectUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    parentObjectService = TestBed.inject(ParentObjectService);
    routeService = TestBed.inject(RouteService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Route query and add missing value', () => {
      const parentObject: IParentObject = { id: 456 };
      const route: IRoute = { id: 1334 };
      parentObject.route = route;

      const routeCollection: IRoute[] = [{ id: 81857 }];
      jest.spyOn(routeService, 'query').mockReturnValue(of(new HttpResponse({ body: routeCollection })));
      const additionalRoutes = [route];
      const expectedCollection: IRoute[] = [...additionalRoutes, ...routeCollection];
      jest.spyOn(routeService, 'addRouteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ parentObject });
      comp.ngOnInit();

      expect(routeService.query).toHaveBeenCalled();
      expect(routeService.addRouteToCollectionIfMissing).toHaveBeenCalledWith(routeCollection, ...additionalRoutes);
      expect(comp.routesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const parentObject: IParentObject = { id: 456 };
      const route: IRoute = { id: 64561 };
      parentObject.route = route;

      activatedRoute.data = of({ parentObject });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(parentObject));
      expect(comp.routesSharedCollection).toContain(route);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ParentObject>>();
      const parentObject = { id: 123 };
      jest.spyOn(parentObjectService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ parentObject });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: parentObject }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(parentObjectService.update).toHaveBeenCalledWith(parentObject);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ParentObject>>();
      const parentObject = new ParentObject();
      jest.spyOn(parentObjectService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ parentObject });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: parentObject }));
      saveSubject.complete();

      // THEN
      expect(parentObjectService.create).toHaveBeenCalledWith(parentObject);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<ParentObject>>();
      const parentObject = { id: 123 };
      jest.spyOn(parentObjectService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ parentObject });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(parentObjectService.update).toHaveBeenCalledWith(parentObject);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackRouteById', () => {
      it('Should return tracked Route primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRouteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
