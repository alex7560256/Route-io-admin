import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import { IParentObject, ParentObject } from '../parent-object.model';
import { ParentObjectService } from '../service/parent-object.service';
import { IRoute } from 'app/entities/route/route.model';
import { RouteService } from 'app/entities/route/service/route.service';

@Component({
  selector: 'jhi-parent-object-update',
  templateUrl: './parent-object-update.component.html',
})
export class ParentObjectUpdateComponent implements OnInit {
  isSaving = false;

  routesSharedCollection: IRoute[] = [];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
    description: [],
    route: [],
  });

  constructor(
    protected parentObjectService: ParentObjectService,
    protected routeService: RouteService,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ parentObject }) => {
      this.updateForm(parentObject);

      this.loadRelationshipsOptions();
    });
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const parentObject = this.createFromForm();
    if (parentObject.id !== undefined) {
      this.subscribeToSaveResponse(this.parentObjectService.update(parentObject));
    } else {
      this.subscribeToSaveResponse(this.parentObjectService.create(parentObject));
    }
  }

  trackRouteById(index: number, item: IRoute): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IParentObject>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(parentObject: IParentObject): void {
    this.editForm.patchValue({
      id: parentObject.id,
      title: parentObject.title,
      description: parentObject.description,
      route: parentObject.route,
    });

    this.routesSharedCollection = this.routeService.addRouteToCollectionIfMissing(this.routesSharedCollection, parentObject.route);
  }

  protected loadRelationshipsOptions(): void {
    this.routeService
      .query()
      .pipe(map((res: HttpResponse<IRoute[]>) => res.body ?? []))
      .pipe(map((routes: IRoute[]) => this.routeService.addRouteToCollectionIfMissing(routes, this.editForm.get('route')!.value)))
      .subscribe((routes: IRoute[]) => (this.routesSharedCollection = routes));
  }

  protected createFromForm(): IParentObject {
    return {
      ...new ParentObject(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      description: this.editForm.get(['description'])!.value,
      route: this.editForm.get(['route'])!.value,
    };
  }
}
