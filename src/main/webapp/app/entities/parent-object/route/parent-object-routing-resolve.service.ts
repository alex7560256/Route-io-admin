import { Injectable } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { Resolve, ActivatedRouteSnapshot, Router } from '@angular/router';
import { Observable, of, EMPTY } from 'rxjs';
import { mergeMap } from 'rxjs/operators';

import { IParentObject, ParentObject } from '../parent-object.model';
import { ParentObjectService } from '../service/parent-object.service';

@Injectable({ providedIn: 'root' })
export class ParentObjectRoutingResolveService implements Resolve<IParentObject> {
  constructor(protected service: ParentObjectService, protected router: Router) {}

  resolve(route: ActivatedRouteSnapshot): Observable<IParentObject> | Observable<never> {
    const id = route.params['id'];
    if (id) {
      return this.service.find(id).pipe(
        mergeMap((parentObject: HttpResponse<ParentObject>) => {
          if (parentObject.body) {
            return of(parentObject.body);
          } else {
            this.router.navigate(['404']);
            return EMPTY;
          }
        })
      );
    }
    return of(new ParentObject());
  }
}
