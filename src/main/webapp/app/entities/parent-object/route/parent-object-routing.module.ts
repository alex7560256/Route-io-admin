import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserRouteAccessService } from 'app/core/auth/user-route-access.service';
import { ParentObjectComponent } from '../list/parent-object.component';
import { ParentObjectDetailComponent } from '../detail/parent-object-detail.component';
import { ParentObjectUpdateComponent } from '../update/parent-object-update.component';
import { ParentObjectRoutingResolveService } from './parent-object-routing-resolve.service';

const parentObjectRoute: Routes = [
  {
    path: '',
    component: ParentObjectComponent,
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/view',
    component: ParentObjectDetailComponent,
    resolve: {
      parentObject: ParentObjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: 'new',
    component: ParentObjectUpdateComponent,
    resolve: {
      parentObject: ParentObjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
  {
    path: ':id/edit',
    component: ParentObjectUpdateComponent,
    resolve: {
      parentObject: ParentObjectRoutingResolveService,
    },
    canActivate: [UserRouteAccessService],
  },
];

@NgModule({
  imports: [RouterModule.forChild(parentObjectRoute)],
  exports: [RouterModule],
})
export class ParentObjectRoutingModule {}
