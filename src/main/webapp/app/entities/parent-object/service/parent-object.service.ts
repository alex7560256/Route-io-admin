import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IParentObject, getParentObjectIdentifier } from '../parent-object.model';

export type EntityResponseType = HttpResponse<IParentObject>;
export type EntityArrayResponseType = HttpResponse<IParentObject[]>;

@Injectable({ providedIn: 'root' })
export class ParentObjectService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/parent-objects');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(parentObject: IParentObject): Observable<EntityResponseType> {
    return this.http.post<IParentObject>(this.resourceUrl, parentObject, { observe: 'response' });
  }

  update(parentObject: IParentObject): Observable<EntityResponseType> {
    return this.http.put<IParentObject>(`${this.resourceUrl}/${getParentObjectIdentifier(parentObject) as number}`, parentObject, {
      observe: 'response',
    });
  }

  partialUpdate(parentObject: IParentObject): Observable<EntityResponseType> {
    return this.http.patch<IParentObject>(`${this.resourceUrl}/${getParentObjectIdentifier(parentObject) as number}`, parentObject, {
      observe: 'response',
    });
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http.get<IParentObject>(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http.get<IParentObject[]>(this.resourceUrl, { params: options, observe: 'response' });
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addParentObjectToCollectionIfMissing(
    parentObjectCollection: IParentObject[],
    ...parentObjectsToCheck: (IParentObject | null | undefined)[]
  ): IParentObject[] {
    const parentObjects: IParentObject[] = parentObjectsToCheck.filter(isPresent);
    if (parentObjects.length > 0) {
      const parentObjectCollectionIdentifiers = parentObjectCollection.map(
        parentObjectItem => getParentObjectIdentifier(parentObjectItem)!
      );
      const parentObjectsToAdd = parentObjects.filter(parentObjectItem => {
        const parentObjectIdentifier = getParentObjectIdentifier(parentObjectItem);
        if (parentObjectIdentifier == null || parentObjectCollectionIdentifiers.includes(parentObjectIdentifier)) {
          return false;
        }
        parentObjectCollectionIdentifiers.push(parentObjectIdentifier);
        return true;
      });
      return [...parentObjectsToAdd, ...parentObjectCollection];
    }
    return parentObjectCollection;
  }
}
