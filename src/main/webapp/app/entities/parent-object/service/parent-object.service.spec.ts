import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { IParentObject, ParentObject } from '../parent-object.model';

import { ParentObjectService } from './parent-object.service';

describe('ParentObject Service', () => {
  let service: ParentObjectService;
  let httpMock: HttpTestingController;
  let elemDefault: IParentObject;
  let expectedResult: IParentObject | IParentObject[] | boolean | null;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
    });
    expectedResult = null;
    service = TestBed.inject(ParentObjectService);
    httpMock = TestBed.inject(HttpTestingController);

    elemDefault = {
      id: 0,
      title: 'AAAAAAA',
      description: 'AAAAAAA',
    };
  });

  describe('Service methods', () => {
    it('should find an element', () => {
      const returnedFromService = Object.assign({}, elemDefault);

      service.find(123).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(elemDefault);
    });

    it('should create a ParentObject', () => {
      const returnedFromService = Object.assign(
        {
          id: 0,
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.create(new ParentObject()).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'POST' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should update a ParentObject', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          title: 'BBBBBB',
          description: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.update(expected).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PUT' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should partial update a ParentObject', () => {
      const patchObject = Object.assign(
        {
          title: 'BBBBBB',
          description: 'BBBBBB',
        },
        new ParentObject()
      );

      const returnedFromService = Object.assign(patchObject, elemDefault);

      const expected = Object.assign({}, returnedFromService);

      service.partialUpdate(patchObject).subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'PATCH' });
      req.flush(returnedFromService);
      expect(expectedResult).toMatchObject(expected);
    });

    it('should return a list of ParentObject', () => {
      const returnedFromService = Object.assign(
        {
          id: 1,
          title: 'BBBBBB',
          description: 'BBBBBB',
        },
        elemDefault
      );

      const expected = Object.assign({}, returnedFromService);

      service.query().subscribe(resp => (expectedResult = resp.body));

      const req = httpMock.expectOne({ method: 'GET' });
      req.flush([returnedFromService]);
      httpMock.verify();
      expect(expectedResult).toContainEqual(expected);
    });

    it('should delete a ParentObject', () => {
      service.delete(123).subscribe(resp => (expectedResult = resp.ok));

      const req = httpMock.expectOne({ method: 'DELETE' });
      req.flush({ status: 200 });
      expect(expectedResult);
    });

    describe('addParentObjectToCollectionIfMissing', () => {
      it('should add a ParentObject to an empty array', () => {
        const parentObject: IParentObject = { id: 123 };
        expectedResult = service.addParentObjectToCollectionIfMissing([], parentObject);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(parentObject);
      });

      it('should not add a ParentObject to an array that contains it', () => {
        const parentObject: IParentObject = { id: 123 };
        const parentObjectCollection: IParentObject[] = [
          {
            ...parentObject,
          },
          { id: 456 },
        ];
        expectedResult = service.addParentObjectToCollectionIfMissing(parentObjectCollection, parentObject);
        expect(expectedResult).toHaveLength(2);
      });

      it("should add a ParentObject to an array that doesn't contain it", () => {
        const parentObject: IParentObject = { id: 123 };
        const parentObjectCollection: IParentObject[] = [{ id: 456 }];
        expectedResult = service.addParentObjectToCollectionIfMissing(parentObjectCollection, parentObject);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(parentObject);
      });

      it('should add only unique ParentObject to an array', () => {
        const parentObjectArray: IParentObject[] = [{ id: 123 }, { id: 456 }, { id: 57195 }];
        const parentObjectCollection: IParentObject[] = [{ id: 123 }];
        expectedResult = service.addParentObjectToCollectionIfMissing(parentObjectCollection, ...parentObjectArray);
        expect(expectedResult).toHaveLength(3);
      });

      it('should accept varargs', () => {
        const parentObject: IParentObject = { id: 123 };
        const parentObject2: IParentObject = { id: 456 };
        expectedResult = service.addParentObjectToCollectionIfMissing([], parentObject, parentObject2);
        expect(expectedResult).toHaveLength(2);
        expect(expectedResult).toContain(parentObject);
        expect(expectedResult).toContain(parentObject2);
      });

      it('should accept null and undefined values', () => {
        const parentObject: IParentObject = { id: 123 };
        expectedResult = service.addParentObjectToCollectionIfMissing([], null, parentObject, undefined);
        expect(expectedResult).toHaveLength(1);
        expect(expectedResult).toContain(parentObject);
      });

      it('should return initial array if no ParentObject is added', () => {
        const parentObjectCollection: IParentObject[] = [{ id: 123 }];
        expectedResult = service.addParentObjectToCollectionIfMissing(parentObjectCollection, undefined, null);
        expect(expectedResult).toEqual(parentObjectCollection);
      });
    });
  });

  afterEach(() => {
    httpMock.verify();
  });
});
