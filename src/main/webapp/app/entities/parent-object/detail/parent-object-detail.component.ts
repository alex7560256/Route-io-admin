import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { IParentObject } from '../parent-object.model';

@Component({
  selector: 'jhi-parent-object-detail',
  templateUrl: './parent-object-detail.component.html',
})
export class ParentObjectDetailComponent implements OnInit {
  parentObject: IParentObject | null = null;

  constructor(protected activatedRoute: ActivatedRoute) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ parentObject }) => {
      this.parentObject = parentObject;
    });
  }

  previousState(): void {
    window.history.back();
  }
}
