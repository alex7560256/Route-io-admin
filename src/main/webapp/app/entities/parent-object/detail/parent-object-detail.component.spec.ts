import { ComponentFixture, TestBed } from '@angular/core/testing';
import { ActivatedRoute } from '@angular/router';
import { of } from 'rxjs';

import { ParentObjectDetailComponent } from './parent-object-detail.component';

describe('ParentObject Management Detail Component', () => {
  let comp: ParentObjectDetailComponent;
  let fixture: ComponentFixture<ParentObjectDetailComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [ParentObjectDetailComponent],
      providers: [
        {
          provide: ActivatedRoute,
          useValue: { data: of({ parentObject: { id: 123 } }) },
        },
      ],
    })
      .overrideTemplate(ParentObjectDetailComponent, '')
      .compileComponents();
    fixture = TestBed.createComponent(ParentObjectDetailComponent);
    comp = fixture.componentInstance;
  });

  describe('OnInit', () => {
    it('Should load parentObject on init', () => {
      // WHEN
      comp.ngOnInit();

      // THEN
      expect(comp.parentObject).toEqual(expect.objectContaining({ id: 123 }));
    });
  });
});
