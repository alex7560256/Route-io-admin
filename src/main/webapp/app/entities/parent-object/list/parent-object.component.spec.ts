import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { ParentObjectService } from '../service/parent-object.service';

import { ParentObjectComponent } from './parent-object.component';

describe('ParentObject Management Component', () => {
  let comp: ParentObjectComponent;
  let fixture: ComponentFixture<ParentObjectComponent>;
  let service: ParentObjectService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [ParentObjectComponent],
    })
      .overrideTemplate(ParentObjectComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(ParentObjectComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(ParentObjectService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.parentObjects?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
