import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IParentObject } from '../parent-object.model';
import { ParentObjectService } from '../service/parent-object.service';
import { ParentObjectDeleteDialogComponent } from '../delete/parent-object-delete-dialog.component';

@Component({
  selector: 'jhi-parent-object',
  templateUrl: './parent-object.component.html',
})
export class ParentObjectComponent implements OnInit {
  parentObjects?: IParentObject[];
  isLoading = false;

  constructor(protected parentObjectService: ParentObjectService, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.parentObjectService.query().subscribe({
      next: (res: HttpResponse<IParentObject[]>) => {
        this.isLoading = false;
        this.parentObjects = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IParentObject): number {
    return item.id!;
  }

  delete(parentObject: IParentObject): void {
    const modalRef = this.modalService.open(ParentObjectDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.parentObject = parentObject;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
