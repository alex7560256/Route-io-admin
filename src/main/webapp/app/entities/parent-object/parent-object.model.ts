import { IRoute } from 'app/entities/route/route.model';

export interface IParentObject {
  id?: number;
  title?: string;
  description?: string | null;
  route?: IRoute | null;
}

export class ParentObject implements IParentObject {
  constructor(public id?: number, public title?: string, public description?: string | null, public route?: IRoute | null) {}
}

export function getParentObjectIdentifier(parentObject: IParentObject): number | undefined {
  return parentObject.id;
}
