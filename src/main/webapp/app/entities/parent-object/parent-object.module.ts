import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ParentObjectComponent } from './list/parent-object.component';
import { ParentObjectDetailComponent } from './detail/parent-object-detail.component';
import { ParentObjectUpdateComponent } from './update/parent-object-update.component';
import { ParentObjectDeleteDialogComponent } from './delete/parent-object-delete-dialog.component';
import { ParentObjectRoutingModule } from './route/parent-object-routing.module';

@NgModule({
  imports: [SharedModule, ParentObjectRoutingModule],
  declarations: [ParentObjectComponent, ParentObjectDetailComponent, ParentObjectUpdateComponent, ParentObjectDeleteDialogComponent],
  entryComponents: [ParentObjectDeleteDialogComponent],
})
export class ParentObjectModule {}
