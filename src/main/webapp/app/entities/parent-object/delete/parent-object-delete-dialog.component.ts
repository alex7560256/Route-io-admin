import { Component } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import { IParentObject } from '../parent-object.model';
import { ParentObjectService } from '../service/parent-object.service';

@Component({
  templateUrl: './parent-object-delete-dialog.component.html',
})
export class ParentObjectDeleteDialogComponent {
  parentObject?: IParentObject;

  constructor(protected parentObjectService: ParentObjectService, protected activeModal: NgbActiveModal) {}

  cancel(): void {
    this.activeModal.dismiss();
  }

  confirmDelete(id: number): void {
    this.parentObjectService.delete(id).subscribe(() => {
      this.activeModal.close('deleted');
    });
  }
}
