import dayjs from 'dayjs/esm';
import { IPoint } from 'app/entities/point/point.model';
import { IMapObject } from 'app/entities/map-object/map-object.model';
import { IParentObject } from 'app/entities/parent-object/parent-object.model';
import { ITag } from 'app/entities/tag/tag.model';
import { ICountry } from 'app/entities/country/country.model';
import { IRegion } from 'app/entities/region/region.model';
import { IState } from 'app/entities/state/state.model';
import { RouteType } from 'app/entities/enumerations/route-type.model';
import { Complexity } from 'app/entities/enumerations/complexity.model';
import { RouteCoverage } from 'app/entities/enumerations/route-coverage.model';

export interface IRoute {
  id?: number;
  title?: string;
  description?: string | null;
  routeType?: RouteType | null;
  photoContentType?: string | null;
  photo?: string | null;
  length?: number;
  looped?: boolean | null;
  accessible?: boolean | null;
  complexity?: Complexity | null;
  price?: string | null;
  days?: number | null;
  hours?: number | null;
  coverage?: RouteCoverage | null;
  create?: dayjs.Dayjs;
  update?: dayjs.Dayjs;
  routePoints?: IPoint[] | null;
  mapObjects?: IMapObject[] | null;
  paentObjects?: IParentObject[] | null;
  tags?: ITag[] | null;
  country?: ICountry | null;
  region?: IRegion | null;
  state?: IState | null;
}

export class Route implements IRoute {
  constructor(
    public id?: number,
    public title?: string,
    public description?: string | null,
    public routeType?: RouteType | null,
    public photoContentType?: string | null,
    public photo?: string | null,
    public length?: number,
    public looped?: boolean | null,
    public accessible?: boolean | null,
    public complexity?: Complexity | null,
    public price?: string | null,
    public days?: number | null,
    public hours?: number | null,
    public coverage?: RouteCoverage | null,
    public create?: dayjs.Dayjs,
    public update?: dayjs.Dayjs,
    public routePoints?: IPoint[] | null,
    public mapObjects?: IMapObject[] | null,
    public paentObjects?: IParentObject[] | null,
    public tags?: ITag[] | null,
    public country?: ICountry | null,
    public region?: IRegion | null,
    public state?: IState | null
  ) {
    this.looped = this.looped ?? false;
    this.accessible = this.accessible ?? false;
  }
}

export function getRouteIdentifier(route: IRoute): number | undefined {
  return route.id;
}
