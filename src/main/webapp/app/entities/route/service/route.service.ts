import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';
import dayjs from 'dayjs/esm';

import { isPresent } from 'app/core/util/operators';
import { ApplicationConfigService } from 'app/core/config/application-config.service';
import { createRequestOption } from 'app/core/request/request-util';
import { IRoute, getRouteIdentifier } from '../route.model';

export type EntityResponseType = HttpResponse<IRoute>;
export type EntityArrayResponseType = HttpResponse<IRoute[]>;

@Injectable({ providedIn: 'root' })
export class RouteService {
  protected resourceUrl = this.applicationConfigService.getEndpointFor('api/routes');

  constructor(protected http: HttpClient, protected applicationConfigService: ApplicationConfigService) {}

  create(route: IRoute): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(route);
    return this.http
      .post<IRoute>(this.resourceUrl, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  update(route: IRoute): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(route);
    return this.http
      .put<IRoute>(`${this.resourceUrl}/${getRouteIdentifier(route) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  partialUpdate(route: IRoute): Observable<EntityResponseType> {
    const copy = this.convertDateFromClient(route);
    return this.http
      .patch<IRoute>(`${this.resourceUrl}/${getRouteIdentifier(route) as number}`, copy, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  find(id: number): Observable<EntityResponseType> {
    return this.http
      .get<IRoute>(`${this.resourceUrl}/${id}`, { observe: 'response' })
      .pipe(map((res: EntityResponseType) => this.convertDateFromServer(res)));
  }

  query(req?: any): Observable<EntityArrayResponseType> {
    const options = createRequestOption(req);
    return this.http
      .get<IRoute[]>(this.resourceUrl, { params: options, observe: 'response' })
      .pipe(map((res: EntityArrayResponseType) => this.convertDateArrayFromServer(res)));
  }

  delete(id: number): Observable<HttpResponse<{}>> {
    return this.http.delete(`${this.resourceUrl}/${id}`, { observe: 'response' });
  }

  addRouteToCollectionIfMissing(routeCollection: IRoute[], ...routesToCheck: (IRoute | null | undefined)[]): IRoute[] {
    const routes: IRoute[] = routesToCheck.filter(isPresent);
    if (routes.length > 0) {
      const routeCollectionIdentifiers = routeCollection.map(routeItem => getRouteIdentifier(routeItem)!);
      const routesToAdd = routes.filter(routeItem => {
        const routeIdentifier = getRouteIdentifier(routeItem);
        if (routeIdentifier == null || routeCollectionIdentifiers.includes(routeIdentifier)) {
          return false;
        }
        routeCollectionIdentifiers.push(routeIdentifier);
        return true;
      });
      return [...routesToAdd, ...routeCollection];
    }
    return routeCollection;
  }

  protected convertDateFromClient(route: IRoute): IRoute {
    return Object.assign({}, route, {
      create: route.create?.isValid() ? route.create.toJSON() : undefined,
      update: route.update?.isValid() ? route.update.toJSON() : undefined,
    });
  }

  protected convertDateFromServer(res: EntityResponseType): EntityResponseType {
    if (res.body) {
      res.body.create = res.body.create ? dayjs(res.body.create) : undefined;
      res.body.update = res.body.update ? dayjs(res.body.update) : undefined;
    }
    return res;
  }

  protected convertDateArrayFromServer(res: EntityArrayResponseType): EntityArrayResponseType {
    if (res.body) {
      res.body.forEach((route: IRoute) => {
        route.create = route.create ? dayjs(route.create) : undefined;
        route.update = route.update ? dayjs(route.update) : undefined;
      });
    }
    return res;
  }
}
