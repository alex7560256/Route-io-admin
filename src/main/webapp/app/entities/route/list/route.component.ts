import { Component, OnInit } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { IRoute } from '../route.model';
import { RouteService } from '../service/route.service';
import { RouteDeleteDialogComponent } from '../delete/route-delete-dialog.component';
import { DataUtils } from 'app/core/util/data-util.service';

@Component({
  selector: 'jhi-route',
  templateUrl: './route.component.html',
})
export class RouteComponent implements OnInit {
  routes?: IRoute[];
  isLoading = false;

  constructor(protected routeService: RouteService, protected dataUtils: DataUtils, protected modalService: NgbModal) {}

  loadAll(): void {
    this.isLoading = true;

    this.routeService.query().subscribe({
      next: (res: HttpResponse<IRoute[]>) => {
        this.isLoading = false;
        this.routes = res.body ?? [];
      },
      error: () => {
        this.isLoading = false;
      },
    });
  }

  ngOnInit(): void {
    this.loadAll();
  }

  trackId(index: number, item: IRoute): number {
    return item.id!;
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    return this.dataUtils.openFile(base64String, contentType);
  }

  delete(route: IRoute): void {
    const modalRef = this.modalService.open(RouteDeleteDialogComponent, { size: 'lg', backdrop: 'static' });
    modalRef.componentInstance.route = route;
    // unsubscribe not needed because closed completes on modal close
    modalRef.closed.subscribe(reason => {
      if (reason === 'deleted') {
        this.loadAll();
      }
    });
  }
}
