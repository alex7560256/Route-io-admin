import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpHeaders, HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { of } from 'rxjs';

import { RouteService } from '../service/route.service';

import { RouteComponent } from './route.component';

describe('Route Management Component', () => {
  let comp: RouteComponent;
  let fixture: ComponentFixture<RouteComponent>;
  let service: RouteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      declarations: [RouteComponent],
    })
      .overrideTemplate(RouteComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(RouteComponent);
    comp = fixture.componentInstance;
    service = TestBed.inject(RouteService);

    const headers = new HttpHeaders();
    jest.spyOn(service, 'query').mockReturnValue(
      of(
        new HttpResponse({
          body: [{ id: 123 }],
          headers,
        })
      )
    );
  });

  it('Should call load all on init', () => {
    // WHEN
    comp.ngOnInit();

    // THEN
    expect(service.query).toHaveBeenCalled();
    expect(comp.routes?.[0]).toEqual(expect.objectContaining({ id: 123 }));
  });
});
