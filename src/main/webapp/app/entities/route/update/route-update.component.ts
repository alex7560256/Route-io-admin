import { Component, OnInit, ElementRef } from '@angular/core';
import { HttpResponse } from '@angular/common/http';
import { FormBuilder, Validators } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { Observable } from 'rxjs';
import { finalize, map } from 'rxjs/operators';

import dayjs from 'dayjs/esm';
import { DATE_TIME_FORMAT } from 'app/config/input.constants';

import { IRoute, Route } from '../route.model';
import { RouteService } from '../service/route.service';
import { AlertError } from 'app/shared/alert/alert-error.model';
import { EventManager, EventWithContent } from 'app/core/util/event-manager.service';
import { DataUtils, FileLoadError } from 'app/core/util/data-util.service';
import { ICountry } from 'app/entities/country/country.model';
import { CountryService } from 'app/entities/country/service/country.service';
import { IRegion } from 'app/entities/region/region.model';
import { RegionService } from 'app/entities/region/service/region.service';
import { IState } from 'app/entities/state/state.model';
import { StateService } from 'app/entities/state/service/state.service';
import { RouteType } from 'app/entities/enumerations/route-type.model';
import { Complexity } from 'app/entities/enumerations/complexity.model';
import { RouteCoverage } from 'app/entities/enumerations/route-coverage.model';

@Component({
  selector: 'jhi-route-update',
  templateUrl: './route-update.component.html',
})
export class RouteUpdateComponent implements OnInit {
  isSaving = false;
  routeTypeValues = Object.keys(RouteType);
  complexityValues = Object.keys(Complexity);
  routeCoverageValues = Object.keys(RouteCoverage);

  countriesSharedCollection: ICountry[] = [];
  regionsSharedCollection: IRegion[] = [];
  statesSharedCollection: IState[] = [];

  editForm = this.fb.group({
    id: [],
    title: [null, [Validators.required]],
    description: [],
    routeType: [],
    photo: [],
    photoContentType: [],
    length: [null, [Validators.required]],
    looped: [],
    accessible: [],
    complexity: [],
    price: [],
    days: [],
    hours: [],
    coverage: [],
    create: [null, [Validators.required]],
    update: [null, [Validators.required]],
    country: [],
    region: [],
    state: [],
  });

  constructor(
    protected dataUtils: DataUtils,
    protected eventManager: EventManager,
    protected routeService: RouteService,
    protected countryService: CountryService,
    protected regionService: RegionService,
    protected stateService: StateService,
    protected elementRef: ElementRef,
    protected activatedRoute: ActivatedRoute,
    protected fb: FormBuilder
  ) {}

  ngOnInit(): void {
    this.activatedRoute.data.subscribe(({ route }) => {
      if (route.id === undefined) {
        const today = dayjs().startOf('day');
        route.create = today;
        route.update = today;
      }

      this.updateForm(route);

      this.loadRelationshipsOptions();
    });
  }

  byteSize(base64String: string): string {
    return this.dataUtils.byteSize(base64String);
  }

  openFile(base64String: string, contentType: string | null | undefined): void {
    this.dataUtils.openFile(base64String, contentType);
  }

  setFileData(event: Event, field: string, isImage: boolean): void {
    this.dataUtils.loadFileToForm(event, this.editForm, field, isImage).subscribe({
      error: (err: FileLoadError) =>
        this.eventManager.broadcast(new EventWithContent<AlertError>('routeApp.error', { ...err, key: 'error.file.' + err.key })),
    });
  }

  clearInputImage(field: string, fieldContentType: string, idInput: string): void {
    this.editForm.patchValue({
      [field]: null,
      [fieldContentType]: null,
    });
    if (idInput && this.elementRef.nativeElement.querySelector('#' + idInput)) {
      this.elementRef.nativeElement.querySelector('#' + idInput).value = null;
    }
  }

  previousState(): void {
    window.history.back();
  }

  save(): void {
    this.isSaving = true;
    const route = this.createFromForm();
    if (route.id !== undefined) {
      this.subscribeToSaveResponse(this.routeService.update(route));
    } else {
      this.subscribeToSaveResponse(this.routeService.create(route));
    }
  }

  trackCountryById(index: number, item: ICountry): number {
    return item.id!;
  }

  trackRegionById(index: number, item: IRegion): number {
    return item.id!;
  }

  trackStateById(index: number, item: IState): number {
    return item.id!;
  }

  protected subscribeToSaveResponse(result: Observable<HttpResponse<IRoute>>): void {
    result.pipe(finalize(() => this.onSaveFinalize())).subscribe({
      next: () => this.onSaveSuccess(),
      error: () => this.onSaveError(),
    });
  }

  protected onSaveSuccess(): void {
    this.previousState();
  }

  protected onSaveError(): void {
    // Api for inheritance.
  }

  protected onSaveFinalize(): void {
    this.isSaving = false;
  }

  protected updateForm(route: IRoute): void {
    this.editForm.patchValue({
      id: route.id,
      title: route.title,
      description: route.description,
      routeType: route.routeType,
      photo: route.photo,
      photoContentType: route.photoContentType,
      length: route.length,
      looped: route.looped,
      accessible: route.accessible,
      complexity: route.complexity,
      price: route.price,
      days: route.days,
      hours: route.hours,
      coverage: route.coverage,
      create: route.create ? route.create.format(DATE_TIME_FORMAT) : null,
      update: route.update ? route.update.format(DATE_TIME_FORMAT) : null,
      country: route.country,
      region: route.region,
      state: route.state,
    });

    this.countriesSharedCollection = this.countryService.addCountryToCollectionIfMissing(this.countriesSharedCollection, route.country);
    this.regionsSharedCollection = this.regionService.addRegionToCollectionIfMissing(this.regionsSharedCollection, route.region);
    this.statesSharedCollection = this.stateService.addStateToCollectionIfMissing(this.statesSharedCollection, route.state);
  }

  protected loadRelationshipsOptions(): void {
    this.countryService
      .query()
      .pipe(map((res: HttpResponse<ICountry[]>) => res.body ?? []))
      .pipe(
        map((countries: ICountry[]) => this.countryService.addCountryToCollectionIfMissing(countries, this.editForm.get('country')!.value))
      )
      .subscribe((countries: ICountry[]) => (this.countriesSharedCollection = countries));

    this.regionService
      .query()
      .pipe(map((res: HttpResponse<IRegion[]>) => res.body ?? []))
      .pipe(map((regions: IRegion[]) => this.regionService.addRegionToCollectionIfMissing(regions, this.editForm.get('region')!.value)))
      .subscribe((regions: IRegion[]) => (this.regionsSharedCollection = regions));

    this.stateService
      .query()
      .pipe(map((res: HttpResponse<IState[]>) => res.body ?? []))
      .pipe(map((states: IState[]) => this.stateService.addStateToCollectionIfMissing(states, this.editForm.get('state')!.value)))
      .subscribe((states: IState[]) => (this.statesSharedCollection = states));
  }

  protected createFromForm(): IRoute {
    return {
      ...new Route(),
      id: this.editForm.get(['id'])!.value,
      title: this.editForm.get(['title'])!.value,
      description: this.editForm.get(['description'])!.value,
      routeType: this.editForm.get(['routeType'])!.value,
      photoContentType: this.editForm.get(['photoContentType'])!.value,
      photo: this.editForm.get(['photo'])!.value,
      length: this.editForm.get(['length'])!.value,
      looped: this.editForm.get(['looped'])!.value,
      accessible: this.editForm.get(['accessible'])!.value,
      complexity: this.editForm.get(['complexity'])!.value,
      price: this.editForm.get(['price'])!.value,
      days: this.editForm.get(['days'])!.value,
      hours: this.editForm.get(['hours'])!.value,
      coverage: this.editForm.get(['coverage'])!.value,
      create: this.editForm.get(['create'])!.value ? dayjs(this.editForm.get(['create'])!.value, DATE_TIME_FORMAT) : undefined,
      update: this.editForm.get(['update'])!.value ? dayjs(this.editForm.get(['update'])!.value, DATE_TIME_FORMAT) : undefined,
      country: this.editForm.get(['country'])!.value,
      region: this.editForm.get(['region'])!.value,
      state: this.editForm.get(['state'])!.value,
    };
  }
}
