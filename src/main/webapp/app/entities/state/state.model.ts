export interface IState {
  id?: number;
  title?: string;
}

export class State implements IState {
  constructor(public id?: number, public title?: string) {}
}

export function getStateIdentifier(state: IState): number | undefined {
  return state.id;
}
