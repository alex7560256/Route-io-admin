export enum RouteCoverage {
  ASPHALT = 'ASPHALT',

  GRAVEL = 'GRAVEL',

  FOREST = 'FOREST',

  SAND = 'SAND',

  GRASS = 'GRASS',

  GRADER = 'GRADER',

  DIRT = 'DIRT',
}
