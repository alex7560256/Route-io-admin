export enum RouteType {
  HIKING = 'HIKING',

  WATER = 'WATER',

  CYCLING = 'CYCLING',

  AUTO = 'AUTO',
}
