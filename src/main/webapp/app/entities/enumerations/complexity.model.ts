export enum Complexity {
  EASY = 'EASY',

  NORMAL = 'NORMAL',

  HARD = 'HARD',
}
