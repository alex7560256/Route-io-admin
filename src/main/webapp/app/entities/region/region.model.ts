export interface IRegion {
  id?: number;
  title?: string;
}

export class Region implements IRegion {
  constructor(public id?: number, public title?: string) {}
}

export function getRegionIdentifier(region: IRegion): number | undefined {
  return region.id;
}
