import { ComponentFixture, TestBed } from '@angular/core/testing';
import { HttpResponse } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { of, Subject, from } from 'rxjs';

import { PointService } from '../service/point.service';
import { IPoint, Point } from '../point.model';
import { IRoute } from 'app/entities/route/route.model';
import { RouteService } from 'app/entities/route/service/route.service';

import { PointUpdateComponent } from './point-update.component';

describe('Point Management Update Component', () => {
  let comp: PointUpdateComponent;
  let fixture: ComponentFixture<PointUpdateComponent>;
  let activatedRoute: ActivatedRoute;
  let pointService: PointService;
  let routeService: RouteService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule, RouterTestingModule.withRoutes([])],
      declarations: [PointUpdateComponent],
      providers: [
        FormBuilder,
        {
          provide: ActivatedRoute,
          useValue: {
            params: from([{}]),
          },
        },
      ],
    })
      .overrideTemplate(PointUpdateComponent, '')
      .compileComponents();

    fixture = TestBed.createComponent(PointUpdateComponent);
    activatedRoute = TestBed.inject(ActivatedRoute);
    pointService = TestBed.inject(PointService);
    routeService = TestBed.inject(RouteService);

    comp = fixture.componentInstance;
  });

  describe('ngOnInit', () => {
    it('Should call Route query and add missing value', () => {
      const point: IPoint = { id: 456 };
      const route: IRoute = { id: 68263 };
      point.route = route;

      const routeCollection: IRoute[] = [{ id: 96998 }];
      jest.spyOn(routeService, 'query').mockReturnValue(of(new HttpResponse({ body: routeCollection })));
      const additionalRoutes = [route];
      const expectedCollection: IRoute[] = [...additionalRoutes, ...routeCollection];
      jest.spyOn(routeService, 'addRouteToCollectionIfMissing').mockReturnValue(expectedCollection);

      activatedRoute.data = of({ point });
      comp.ngOnInit();

      expect(routeService.query).toHaveBeenCalled();
      expect(routeService.addRouteToCollectionIfMissing).toHaveBeenCalledWith(routeCollection, ...additionalRoutes);
      expect(comp.routesSharedCollection).toEqual(expectedCollection);
    });

    it('Should update editForm', () => {
      const point: IPoint = { id: 456 };
      const route: IRoute = { id: 5144 };
      point.route = route;

      activatedRoute.data = of({ point });
      comp.ngOnInit();

      expect(comp.editForm.value).toEqual(expect.objectContaining(point));
      expect(comp.routesSharedCollection).toContain(route);
    });
  });

  describe('save', () => {
    it('Should call update service on save for existing entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Point>>();
      const point = { id: 123 };
      jest.spyOn(pointService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ point });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: point }));
      saveSubject.complete();

      // THEN
      expect(comp.previousState).toHaveBeenCalled();
      expect(pointService.update).toHaveBeenCalledWith(point);
      expect(comp.isSaving).toEqual(false);
    });

    it('Should call create service on save for new entity', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Point>>();
      const point = new Point();
      jest.spyOn(pointService, 'create').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ point });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.next(new HttpResponse({ body: point }));
      saveSubject.complete();

      // THEN
      expect(pointService.create).toHaveBeenCalledWith(point);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).toHaveBeenCalled();
    });

    it('Should set isSaving to false on error', () => {
      // GIVEN
      const saveSubject = new Subject<HttpResponse<Point>>();
      const point = { id: 123 };
      jest.spyOn(pointService, 'update').mockReturnValue(saveSubject);
      jest.spyOn(comp, 'previousState');
      activatedRoute.data = of({ point });
      comp.ngOnInit();

      // WHEN
      comp.save();
      expect(comp.isSaving).toEqual(true);
      saveSubject.error('This is an error!');

      // THEN
      expect(pointService.update).toHaveBeenCalledWith(point);
      expect(comp.isSaving).toEqual(false);
      expect(comp.previousState).not.toHaveBeenCalled();
    });
  });

  describe('Tracking relationships identifiers', () => {
    describe('trackRouteById', () => {
      it('Should return tracked Route primary key', () => {
        const entity = { id: 123 };
        const trackResult = comp.trackRouteById(0, entity);
        expect(trackResult).toEqual(entity.id);
      });
    });
  });
});
