import { IRoute } from 'app/entities/route/route.model';

export interface IPoint {
  id?: number;
  lat?: number;
  lang?: number;
  number?: number;
  route?: IRoute | null;
}

export class Point implements IPoint {
  constructor(public id?: number, public lat?: number, public lang?: number, public number?: number, public route?: IRoute | null) {}
}

export function getPointIdentifier(point: IPoint): number | undefined {
  return point.id;
}
