package io.route.admin.repository;

import io.route.admin.domain.MapObjectType;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the MapObjectType entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MapObjectTypeRepository extends JpaRepository<MapObjectType, Long> {}
