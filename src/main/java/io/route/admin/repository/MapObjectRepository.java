package io.route.admin.repository;

import io.route.admin.domain.MapObject;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the MapObject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface MapObjectRepository extends JpaRepository<MapObject, Long> {}
