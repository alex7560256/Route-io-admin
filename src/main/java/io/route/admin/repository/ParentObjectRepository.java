package io.route.admin.repository;

import io.route.admin.domain.ParentObject;
import org.springframework.data.jpa.repository.*;
import org.springframework.stereotype.Repository;

/**
 * Spring Data SQL repository for the ParentObject entity.
 */
@SuppressWarnings("unused")
@Repository
public interface ParentObjectRepository extends JpaRepository<ParentObject, Long> {}
