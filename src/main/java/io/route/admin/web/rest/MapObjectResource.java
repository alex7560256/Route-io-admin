package io.route.admin.web.rest;

import io.route.admin.domain.MapObject;
import io.route.admin.repository.MapObjectRepository;
import io.route.admin.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link io.route.admin.domain.MapObject}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MapObjectResource {

    private final Logger log = LoggerFactory.getLogger(MapObjectResource.class);

    private static final String ENTITY_NAME = "mapObject";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MapObjectRepository mapObjectRepository;

    public MapObjectResource(MapObjectRepository mapObjectRepository) {
        this.mapObjectRepository = mapObjectRepository;
    }

    /**
     * {@code POST  /map-objects} : Create a new mapObject.
     *
     * @param mapObject the mapObject to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mapObject, or with status {@code 400 (Bad Request)} if the mapObject has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/map-objects")
    public ResponseEntity<MapObject> createMapObject(@Valid @RequestBody MapObject mapObject) throws URISyntaxException {
        log.debug("REST request to save MapObject : {}", mapObject);
        if (mapObject.getId() != null) {
            throw new BadRequestAlertException("A new mapObject cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MapObject result = mapObjectRepository.save(mapObject);
        return ResponseEntity
            .created(new URI("/api/map-objects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /map-objects/:id} : Updates an existing mapObject.
     *
     * @param id the id of the mapObject to save.
     * @param mapObject the mapObject to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mapObject,
     * or with status {@code 400 (Bad Request)} if the mapObject is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mapObject couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/map-objects/{id}")
    public ResponseEntity<MapObject> updateMapObject(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MapObject mapObject
    ) throws URISyntaxException {
        log.debug("REST request to update MapObject : {}, {}", id, mapObject);
        if (mapObject.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, mapObject.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!mapObjectRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MapObject result = mapObjectRepository.save(mapObject);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mapObject.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /map-objects/:id} : Partial updates given fields of an existing mapObject, field will ignore if it is null
     *
     * @param id the id of the mapObject to save.
     * @param mapObject the mapObject to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mapObject,
     * or with status {@code 400 (Bad Request)} if the mapObject is not valid,
     * or with status {@code 404 (Not Found)} if the mapObject is not found,
     * or with status {@code 500 (Internal Server Error)} if the mapObject couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/map-objects/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MapObject> partialUpdateMapObject(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MapObject mapObject
    ) throws URISyntaxException {
        log.debug("REST request to partial update MapObject partially : {}, {}", id, mapObject);
        if (mapObject.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, mapObject.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!mapObjectRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MapObject> result = mapObjectRepository
            .findById(mapObject.getId())
            .map(existingMapObject -> {
                if (mapObject.getTitle() != null) {
                    existingMapObject.setTitle(mapObject.getTitle());
                }
                if (mapObject.getDescription() != null) {
                    existingMapObject.setDescription(mapObject.getDescription());
                }
                if (mapObject.getLat() != null) {
                    existingMapObject.setLat(mapObject.getLat());
                }
                if (mapObject.getLang() != null) {
                    existingMapObject.setLang(mapObject.getLang());
                }
                if (mapObject.getPhoto() != null) {
                    existingMapObject.setPhoto(mapObject.getPhoto());
                }
                if (mapObject.getPhotoContentType() != null) {
                    existingMapObject.setPhotoContentType(mapObject.getPhotoContentType());
                }
                if (mapObject.getConstructionsDate() != null) {
                    existingMapObject.setConstructionsDate(mapObject.getConstructionsDate());
                }
                if (mapObject.getPaid() != null) {
                    existingMapObject.setPaid(mapObject.getPaid());
                }
                if (mapObject.getCost() != null) {
                    existingMapObject.setCost(mapObject.getCost());
                }
                if (mapObject.getAltitude() != null) {
                    existingMapObject.setAltitude(mapObject.getAltitude());
                }
                if (mapObject.getCreate() != null) {
                    existingMapObject.setCreate(mapObject.getCreate());
                }
                if (mapObject.getUpdate() != null) {
                    existingMapObject.setUpdate(mapObject.getUpdate());
                }

                return existingMapObject;
            })
            .map(mapObjectRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mapObject.getId().toString())
        );
    }

    /**
     * {@code GET  /map-objects} : get all the mapObjects.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mapObjects in body.
     */
    @GetMapping("/map-objects")
    public List<MapObject> getAllMapObjects() {
        log.debug("REST request to get all MapObjects");
        return mapObjectRepository.findAll();
    }

    /**
     * {@code GET  /map-objects/:id} : get the "id" mapObject.
     *
     * @param id the id of the mapObject to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mapObject, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/map-objects/{id}")
    public ResponseEntity<MapObject> getMapObject(@PathVariable Long id) {
        log.debug("REST request to get MapObject : {}", id);
        Optional<MapObject> mapObject = mapObjectRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(mapObject);
    }

    /**
     * {@code DELETE  /map-objects/:id} : delete the "id" mapObject.
     *
     * @param id the id of the mapObject to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/map-objects/{id}")
    public ResponseEntity<Void> deleteMapObject(@PathVariable Long id) {
        log.debug("REST request to delete MapObject : {}", id);
        mapObjectRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
