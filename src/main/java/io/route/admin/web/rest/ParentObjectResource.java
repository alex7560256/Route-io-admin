package io.route.admin.web.rest;

import io.route.admin.domain.ParentObject;
import io.route.admin.repository.ParentObjectRepository;
import io.route.admin.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link io.route.admin.domain.ParentObject}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class ParentObjectResource {

    private final Logger log = LoggerFactory.getLogger(ParentObjectResource.class);

    private static final String ENTITY_NAME = "parentObject";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final ParentObjectRepository parentObjectRepository;

    public ParentObjectResource(ParentObjectRepository parentObjectRepository) {
        this.parentObjectRepository = parentObjectRepository;
    }

    /**
     * {@code POST  /parent-objects} : Create a new parentObject.
     *
     * @param parentObject the parentObject to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new parentObject, or with status {@code 400 (Bad Request)} if the parentObject has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/parent-objects")
    public ResponseEntity<ParentObject> createParentObject(@Valid @RequestBody ParentObject parentObject) throws URISyntaxException {
        log.debug("REST request to save ParentObject : {}", parentObject);
        if (parentObject.getId() != null) {
            throw new BadRequestAlertException("A new parentObject cannot already have an ID", ENTITY_NAME, "idexists");
        }
        ParentObject result = parentObjectRepository.save(parentObject);
        return ResponseEntity
            .created(new URI("/api/parent-objects/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /parent-objects/:id} : Updates an existing parentObject.
     *
     * @param id the id of the parentObject to save.
     * @param parentObject the parentObject to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated parentObject,
     * or with status {@code 400 (Bad Request)} if the parentObject is not valid,
     * or with status {@code 500 (Internal Server Error)} if the parentObject couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/parent-objects/{id}")
    public ResponseEntity<ParentObject> updateParentObject(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody ParentObject parentObject
    ) throws URISyntaxException {
        log.debug("REST request to update ParentObject : {}, {}", id, parentObject);
        if (parentObject.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, parentObject.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!parentObjectRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        ParentObject result = parentObjectRepository.save(parentObject);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, parentObject.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /parent-objects/:id} : Partial updates given fields of an existing parentObject, field will ignore if it is null
     *
     * @param id the id of the parentObject to save.
     * @param parentObject the parentObject to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated parentObject,
     * or with status {@code 400 (Bad Request)} if the parentObject is not valid,
     * or with status {@code 404 (Not Found)} if the parentObject is not found,
     * or with status {@code 500 (Internal Server Error)} if the parentObject couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/parent-objects/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<ParentObject> partialUpdateParentObject(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody ParentObject parentObject
    ) throws URISyntaxException {
        log.debug("REST request to partial update ParentObject partially : {}, {}", id, parentObject);
        if (parentObject.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, parentObject.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!parentObjectRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<ParentObject> result = parentObjectRepository
            .findById(parentObject.getId())
            .map(existingParentObject -> {
                if (parentObject.getTitle() != null) {
                    existingParentObject.setTitle(parentObject.getTitle());
                }
                if (parentObject.getDescription() != null) {
                    existingParentObject.setDescription(parentObject.getDescription());
                }

                return existingParentObject;
            })
            .map(parentObjectRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, parentObject.getId().toString())
        );
    }

    /**
     * {@code GET  /parent-objects} : get all the parentObjects.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of parentObjects in body.
     */
    @GetMapping("/parent-objects")
    public List<ParentObject> getAllParentObjects() {
        log.debug("REST request to get all ParentObjects");
        return parentObjectRepository.findAll();
    }

    /**
     * {@code GET  /parent-objects/:id} : get the "id" parentObject.
     *
     * @param id the id of the parentObject to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the parentObject, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/parent-objects/{id}")
    public ResponseEntity<ParentObject> getParentObject(@PathVariable Long id) {
        log.debug("REST request to get ParentObject : {}", id);
        Optional<ParentObject> parentObject = parentObjectRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(parentObject);
    }

    /**
     * {@code DELETE  /parent-objects/:id} : delete the "id" parentObject.
     *
     * @param id the id of the parentObject to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/parent-objects/{id}")
    public ResponseEntity<Void> deleteParentObject(@PathVariable Long id) {
        log.debug("REST request to delete ParentObject : {}", id);
        parentObjectRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
