/**
 * View Models used by Spring MVC REST controllers.
 */
package io.route.admin.web.rest.vm;
