package io.route.admin.web.rest;

import io.route.admin.domain.MapObjectType;
import io.route.admin.repository.MapObjectTypeRepository;
import io.route.admin.web.rest.errors.BadRequestAlertException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import javax.validation.Valid;
import javax.validation.constraints.NotNull;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.*;
import tech.jhipster.web.util.HeaderUtil;
import tech.jhipster.web.util.ResponseUtil;

/**
 * REST controller for managing {@link io.route.admin.domain.MapObjectType}.
 */
@RestController
@RequestMapping("/api")
@Transactional
public class MapObjectTypeResource {

    private final Logger log = LoggerFactory.getLogger(MapObjectTypeResource.class);

    private static final String ENTITY_NAME = "mapObjectType";

    @Value("${jhipster.clientApp.name}")
    private String applicationName;

    private final MapObjectTypeRepository mapObjectTypeRepository;

    public MapObjectTypeResource(MapObjectTypeRepository mapObjectTypeRepository) {
        this.mapObjectTypeRepository = mapObjectTypeRepository;
    }

    /**
     * {@code POST  /map-object-types} : Create a new mapObjectType.
     *
     * @param mapObjectType the mapObjectType to create.
     * @return the {@link ResponseEntity} with status {@code 201 (Created)} and with body the new mapObjectType, or with status {@code 400 (Bad Request)} if the mapObjectType has already an ID.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PostMapping("/map-object-types")
    public ResponseEntity<MapObjectType> createMapObjectType(@Valid @RequestBody MapObjectType mapObjectType) throws URISyntaxException {
        log.debug("REST request to save MapObjectType : {}", mapObjectType);
        if (mapObjectType.getId() != null) {
            throw new BadRequestAlertException("A new mapObjectType cannot already have an ID", ENTITY_NAME, "idexists");
        }
        MapObjectType result = mapObjectTypeRepository.save(mapObjectType);
        return ResponseEntity
            .created(new URI("/api/map-object-types/" + result.getId()))
            .headers(HeaderUtil.createEntityCreationAlert(applicationName, true, ENTITY_NAME, result.getId().toString()))
            .body(result);
    }

    /**
     * {@code PUT  /map-object-types/:id} : Updates an existing mapObjectType.
     *
     * @param id the id of the mapObjectType to save.
     * @param mapObjectType the mapObjectType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mapObjectType,
     * or with status {@code 400 (Bad Request)} if the mapObjectType is not valid,
     * or with status {@code 500 (Internal Server Error)} if the mapObjectType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PutMapping("/map-object-types/{id}")
    public ResponseEntity<MapObjectType> updateMapObjectType(
        @PathVariable(value = "id", required = false) final Long id,
        @Valid @RequestBody MapObjectType mapObjectType
    ) throws URISyntaxException {
        log.debug("REST request to update MapObjectType : {}, {}", id, mapObjectType);
        if (mapObjectType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, mapObjectType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!mapObjectTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        MapObjectType result = mapObjectTypeRepository.save(mapObjectType);
        return ResponseEntity
            .ok()
            .headers(HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mapObjectType.getId().toString()))
            .body(result);
    }

    /**
     * {@code PATCH  /map-object-types/:id} : Partial updates given fields of an existing mapObjectType, field will ignore if it is null
     *
     * @param id the id of the mapObjectType to save.
     * @param mapObjectType the mapObjectType to update.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the updated mapObjectType,
     * or with status {@code 400 (Bad Request)} if the mapObjectType is not valid,
     * or with status {@code 404 (Not Found)} if the mapObjectType is not found,
     * or with status {@code 500 (Internal Server Error)} if the mapObjectType couldn't be updated.
     * @throws URISyntaxException if the Location URI syntax is incorrect.
     */
    @PatchMapping(value = "/map-object-types/{id}", consumes = { "application/json", "application/merge-patch+json" })
    public ResponseEntity<MapObjectType> partialUpdateMapObjectType(
        @PathVariable(value = "id", required = false) final Long id,
        @NotNull @RequestBody MapObjectType mapObjectType
    ) throws URISyntaxException {
        log.debug("REST request to partial update MapObjectType partially : {}, {}", id, mapObjectType);
        if (mapObjectType.getId() == null) {
            throw new BadRequestAlertException("Invalid id", ENTITY_NAME, "idnull");
        }
        if (!Objects.equals(id, mapObjectType.getId())) {
            throw new BadRequestAlertException("Invalid ID", ENTITY_NAME, "idinvalid");
        }

        if (!mapObjectTypeRepository.existsById(id)) {
            throw new BadRequestAlertException("Entity not found", ENTITY_NAME, "idnotfound");
        }

        Optional<MapObjectType> result = mapObjectTypeRepository
            .findById(mapObjectType.getId())
            .map(existingMapObjectType -> {
                if (mapObjectType.getTitle() != null) {
                    existingMapObjectType.setTitle(mapObjectType.getTitle());
                }

                return existingMapObjectType;
            })
            .map(mapObjectTypeRepository::save);

        return ResponseUtil.wrapOrNotFound(
            result,
            HeaderUtil.createEntityUpdateAlert(applicationName, true, ENTITY_NAME, mapObjectType.getId().toString())
        );
    }

    /**
     * {@code GET  /map-object-types} : get all the mapObjectTypes.
     *
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and the list of mapObjectTypes in body.
     */
    @GetMapping("/map-object-types")
    public List<MapObjectType> getAllMapObjectTypes() {
        log.debug("REST request to get all MapObjectTypes");
        return mapObjectTypeRepository.findAll();
    }

    /**
     * {@code GET  /map-object-types/:id} : get the "id" mapObjectType.
     *
     * @param id the id of the mapObjectType to retrieve.
     * @return the {@link ResponseEntity} with status {@code 200 (OK)} and with body the mapObjectType, or with status {@code 404 (Not Found)}.
     */
    @GetMapping("/map-object-types/{id}")
    public ResponseEntity<MapObjectType> getMapObjectType(@PathVariable Long id) {
        log.debug("REST request to get MapObjectType : {}", id);
        Optional<MapObjectType> mapObjectType = mapObjectTypeRepository.findById(id);
        return ResponseUtil.wrapOrNotFound(mapObjectType);
    }

    /**
     * {@code DELETE  /map-object-types/:id} : delete the "id" mapObjectType.
     *
     * @param id the id of the mapObjectType to delete.
     * @return the {@link ResponseEntity} with status {@code 204 (NO_CONTENT)}.
     */
    @DeleteMapping("/map-object-types/{id}")
    public ResponseEntity<Void> deleteMapObjectType(@PathVariable Long id) {
        log.debug("REST request to delete MapObjectType : {}", id);
        mapObjectTypeRepository.deleteById(id);
        return ResponseEntity
            .noContent()
            .headers(HeaderUtil.createEntityDeletionAlert(applicationName, true, ENTITY_NAME, id.toString()))
            .build();
    }
}
