package io.route.admin.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import java.time.LocalDate;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A MapObject.
 */
@Entity
@Table(name = "map_object")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class MapObject implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @NotNull
    @Column(name = "lat", nullable = false)
    private Long lat;

    @NotNull
    @Column(name = "lang", nullable = false)
    private Long lang;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @Column(name = "constructions_date")
    private LocalDate constructionsDate;

    @Column(name = "paid")
    private Boolean paid;

    @Column(name = "cost")
    private Long cost;

    @Column(name = "altitude")
    private Long altitude;

    @NotNull
    @Column(name = "jhi_create", nullable = false)
    private ZonedDateTime create;

    @NotNull
    @Column(name = "update", nullable = false)
    private ZonedDateTime update;

    @OneToMany(mappedBy = "mapObject")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "route", "mapObject" }, allowSetters = true)
    private Set<Tag> tags = new HashSet<>();

    @ManyToOne
    private Country country;

    @ManyToOne
    private Region region;

    @ManyToOne
    private State state;

    @ManyToOne
    private MapObjectType type;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "routePoints", "mapObjects", "paentObjects", "tags", "country", "region", "state" },
        allowSetters = true
    )
    private Route route;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public MapObject id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public MapObject title(String title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public MapObject description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Long getLat() {
        return this.lat;
    }

    public MapObject lat(Long lat) {
        this.setLat(lat);
        return this;
    }

    public void setLat(Long lat) {
        this.lat = lat;
    }

    public Long getLang() {
        return this.lang;
    }

    public MapObject lang(Long lang) {
        this.setLang(lang);
        return this;
    }

    public void setLang(Long lang) {
        this.lang = lang;
    }

    public byte[] getPhoto() {
        return this.photo;
    }

    public MapObject photo(byte[] photo) {
        this.setPhoto(photo);
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return this.photoContentType;
    }

    public MapObject photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public LocalDate getConstructionsDate() {
        return this.constructionsDate;
    }

    public MapObject constructionsDate(LocalDate constructionsDate) {
        this.setConstructionsDate(constructionsDate);
        return this;
    }

    public void setConstructionsDate(LocalDate constructionsDate) {
        this.constructionsDate = constructionsDate;
    }

    public Boolean getPaid() {
        return this.paid;
    }

    public MapObject paid(Boolean paid) {
        this.setPaid(paid);
        return this;
    }

    public void setPaid(Boolean paid) {
        this.paid = paid;
    }

    public Long getCost() {
        return this.cost;
    }

    public MapObject cost(Long cost) {
        this.setCost(cost);
        return this;
    }

    public void setCost(Long cost) {
        this.cost = cost;
    }

    public Long getAltitude() {
        return this.altitude;
    }

    public MapObject altitude(Long altitude) {
        this.setAltitude(altitude);
        return this;
    }

    public void setAltitude(Long altitude) {
        this.altitude = altitude;
    }

    public ZonedDateTime getCreate() {
        return this.create;
    }

    public MapObject create(ZonedDateTime create) {
        this.setCreate(create);
        return this;
    }

    public void setCreate(ZonedDateTime create) {
        this.create = create;
    }

    public ZonedDateTime getUpdate() {
        return this.update;
    }

    public MapObject update(ZonedDateTime update) {
        this.setUpdate(update);
        return this;
    }

    public void setUpdate(ZonedDateTime update) {
        this.update = update;
    }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public void setTags(Set<Tag> tags) {
        if (this.tags != null) {
            this.tags.forEach(i -> i.setMapObject(null));
        }
        if (tags != null) {
            tags.forEach(i -> i.setMapObject(this));
        }
        this.tags = tags;
    }

    public MapObject tags(Set<Tag> tags) {
        this.setTags(tags);
        return this;
    }

    public MapObject addTags(Tag tag) {
        this.tags.add(tag);
        tag.setMapObject(this);
        return this;
    }

    public MapObject removeTags(Tag tag) {
        this.tags.remove(tag);
        tag.setMapObject(null);
        return this;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public MapObject country(Country country) {
        this.setCountry(country);
        return this;
    }

    public Region getRegion() {
        return this.region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public MapObject region(Region region) {
        this.setRegion(region);
        return this;
    }

    public State getState() {
        return this.state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public MapObject state(State state) {
        this.setState(state);
        return this;
    }

    public MapObjectType getType() {
        return this.type;
    }

    public void setType(MapObjectType mapObjectType) {
        this.type = mapObjectType;
    }

    public MapObject type(MapObjectType mapObjectType) {
        this.setType(mapObjectType);
        return this;
    }

    public Route getRoute() {
        return this.route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public MapObject route(Route route) {
        this.setRoute(route);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof MapObject)) {
            return false;
        }
        return id != null && id.equals(((MapObject) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "MapObject{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", lat=" + getLat() +
            ", lang=" + getLang() +
            ", photo='" + getPhoto() + "'" +
            ", photoContentType='" + getPhotoContentType() + "'" +
            ", constructionsDate='" + getConstructionsDate() + "'" +
            ", paid='" + getPaid() + "'" +
            ", cost=" + getCost() +
            ", altitude=" + getAltitude() +
            ", create='" + getCreate() + "'" +
            ", update='" + getUpdate() + "'" +
            "}";
    }
}
