package io.route.admin.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Point.
 */
@Entity
@Table(name = "point")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Point implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "lat", nullable = false)
    private Long lat;

    @NotNull
    @Column(name = "lang", nullable = false)
    private Long lang;

    @NotNull
    @Column(name = "number", nullable = false)
    private Integer number;

    @ManyToOne
    @JsonIgnoreProperties(
        value = { "routePoints", "mapObjects", "paentObjects", "tags", "country", "region", "state" },
        allowSetters = true
    )
    private Route route;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Point id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Long getLat() {
        return this.lat;
    }

    public Point lat(Long lat) {
        this.setLat(lat);
        return this;
    }

    public void setLat(Long lat) {
        this.lat = lat;
    }

    public Long getLang() {
        return this.lang;
    }

    public Point lang(Long lang) {
        this.setLang(lang);
        return this;
    }

    public void setLang(Long lang) {
        this.lang = lang;
    }

    public Integer getNumber() {
        return this.number;
    }

    public Point number(Integer number) {
        this.setNumber(number);
        return this;
    }

    public void setNumber(Integer number) {
        this.number = number;
    }

    public Route getRoute() {
        return this.route;
    }

    public void setRoute(Route route) {
        this.route = route;
    }

    public Point route(Route route) {
        this.setRoute(route);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Point)) {
            return false;
        }
        return id != null && id.equals(((Point) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Point{" +
            "id=" + getId() +
            ", lat=" + getLat() +
            ", lang=" + getLang() +
            ", number=" + getNumber() +
            "}";
    }
}
