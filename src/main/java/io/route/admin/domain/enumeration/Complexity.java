package io.route.admin.domain.enumeration;

/**
 * The Complexity enumeration.
 */
public enum Complexity {
    EASY,
    NORMAL,
    HARD,
}
