package io.route.admin.domain.enumeration;

/**
 * The RouteType enumeration.
 */
public enum RouteType {
    HIKING,
    WATER,
    CYCLING,
    AUTO,
}
