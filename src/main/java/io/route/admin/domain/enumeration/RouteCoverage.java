package io.route.admin.domain.enumeration;

/**
 * The RouteCoverage enumeration.
 */
public enum RouteCoverage {
    ASPHALT,
    GRAVEL,
    FOREST,
    SAND,
    GRASS,
    GRADER,
    DIRT,
}
