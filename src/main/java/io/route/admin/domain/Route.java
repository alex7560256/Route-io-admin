package io.route.admin.domain;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import io.route.admin.domain.enumeration.Complexity;
import io.route.admin.domain.enumeration.RouteCoverage;
import io.route.admin.domain.enumeration.RouteType;
import java.io.Serializable;
import java.time.ZonedDateTime;
import java.util.HashSet;
import java.util.Set;
import javax.persistence.*;
import javax.validation.constraints.*;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * A Route.
 */
@Entity
@Table(name = "route")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
public class Route implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "sequenceGenerator")
    @SequenceGenerator(name = "sequenceGenerator")
    @Column(name = "id")
    private Long id;

    @NotNull
    @Column(name = "title", nullable = false)
    private String title;

    @Column(name = "description")
    private String description;

    @Enumerated(EnumType.STRING)
    @Column(name = "route_type")
    private RouteType routeType;

    @Lob
    @Column(name = "photo")
    private byte[] photo;

    @Column(name = "photo_content_type")
    private String photoContentType;

    @NotNull
    @Column(name = "length", nullable = false)
    private Long length;

    @Column(name = "looped")
    private Boolean looped;

    @Column(name = "accessible")
    private Boolean accessible;

    @Enumerated(EnumType.STRING)
    @Column(name = "complexity")
    private Complexity complexity;

    @Column(name = "price")
    private String price;

    @Column(name = "days")
    private Integer days;

    @Column(name = "hours")
    private Integer hours;

    @Enumerated(EnumType.STRING)
    @Column(name = "coverage")
    private RouteCoverage coverage;

    @NotNull
    @Column(name = "jhi_create", nullable = false)
    private ZonedDateTime create;

    @NotNull
    @Column(name = "update", nullable = false)
    private ZonedDateTime update;

    @OneToMany(mappedBy = "route")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "route" }, allowSetters = true)
    private Set<Point> routePoints = new HashSet<>();

    @OneToMany(mappedBy = "route")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "tags", "country", "region", "state", "type", "route" }, allowSetters = true)
    private Set<MapObject> mapObjects = new HashSet<>();

    @OneToMany(mappedBy = "route")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "route" }, allowSetters = true)
    private Set<ParentObject> paentObjects = new HashSet<>();

    @OneToMany(mappedBy = "route")
    @Cache(usage = CacheConcurrencyStrategy.READ_WRITE)
    @JsonIgnoreProperties(value = { "route", "mapObject" }, allowSetters = true)
    private Set<Tag> tags = new HashSet<>();

    @ManyToOne
    private Country country;

    @ManyToOne
    private Region region;

    @ManyToOne
    private State state;

    // jhipster-needle-entity-add-field - JHipster will add fields here

    public Long getId() {
        return this.id;
    }

    public Route id(Long id) {
        this.setId(id);
        return this;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return this.title;
    }

    public Route title(String title) {
        this.setTitle(title);
        return this;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getDescription() {
        return this.description;
    }

    public Route description(String description) {
        this.setDescription(description);
        return this;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public RouteType getRouteType() {
        return this.routeType;
    }

    public Route routeType(RouteType routeType) {
        this.setRouteType(routeType);
        return this;
    }

    public void setRouteType(RouteType routeType) {
        this.routeType = routeType;
    }

    public byte[] getPhoto() {
        return this.photo;
    }

    public Route photo(byte[] photo) {
        this.setPhoto(photo);
        return this;
    }

    public void setPhoto(byte[] photo) {
        this.photo = photo;
    }

    public String getPhotoContentType() {
        return this.photoContentType;
    }

    public Route photoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
        return this;
    }

    public void setPhotoContentType(String photoContentType) {
        this.photoContentType = photoContentType;
    }

    public Long getLength() {
        return this.length;
    }

    public Route length(Long length) {
        this.setLength(length);
        return this;
    }

    public void setLength(Long length) {
        this.length = length;
    }

    public Boolean getLooped() {
        return this.looped;
    }

    public Route looped(Boolean looped) {
        this.setLooped(looped);
        return this;
    }

    public void setLooped(Boolean looped) {
        this.looped = looped;
    }

    public Boolean getAccessible() {
        return this.accessible;
    }

    public Route accessible(Boolean accessible) {
        this.setAccessible(accessible);
        return this;
    }

    public void setAccessible(Boolean accessible) {
        this.accessible = accessible;
    }

    public Complexity getComplexity() {
        return this.complexity;
    }

    public Route complexity(Complexity complexity) {
        this.setComplexity(complexity);
        return this;
    }

    public void setComplexity(Complexity complexity) {
        this.complexity = complexity;
    }

    public String getPrice() {
        return this.price;
    }

    public Route price(String price) {
        this.setPrice(price);
        return this;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public Integer getDays() {
        return this.days;
    }

    public Route days(Integer days) {
        this.setDays(days);
        return this;
    }

    public void setDays(Integer days) {
        this.days = days;
    }

    public Integer getHours() {
        return this.hours;
    }

    public Route hours(Integer hours) {
        this.setHours(hours);
        return this;
    }

    public void setHours(Integer hours) {
        this.hours = hours;
    }

    public RouteCoverage getCoverage() {
        return this.coverage;
    }

    public Route coverage(RouteCoverage coverage) {
        this.setCoverage(coverage);
        return this;
    }

    public void setCoverage(RouteCoverage coverage) {
        this.coverage = coverage;
    }

    public ZonedDateTime getCreate() {
        return this.create;
    }

    public Route create(ZonedDateTime create) {
        this.setCreate(create);
        return this;
    }

    public void setCreate(ZonedDateTime create) {
        this.create = create;
    }

    public ZonedDateTime getUpdate() {
        return this.update;
    }

    public Route update(ZonedDateTime update) {
        this.setUpdate(update);
        return this;
    }

    public void setUpdate(ZonedDateTime update) {
        this.update = update;
    }

    public Set<Point> getRoutePoints() {
        return this.routePoints;
    }

    public void setRoutePoints(Set<Point> points) {
        if (this.routePoints != null) {
            this.routePoints.forEach(i -> i.setRoute(null));
        }
        if (points != null) {
            points.forEach(i -> i.setRoute(this));
        }
        this.routePoints = points;
    }

    public Route routePoints(Set<Point> points) {
        this.setRoutePoints(points);
        return this;
    }

    public Route addRoutePoints(Point point) {
        this.routePoints.add(point);
        point.setRoute(this);
        return this;
    }

    public Route removeRoutePoints(Point point) {
        this.routePoints.remove(point);
        point.setRoute(null);
        return this;
    }

    public Set<MapObject> getMapObjects() {
        return this.mapObjects;
    }

    public void setMapObjects(Set<MapObject> mapObjects) {
        if (this.mapObjects != null) {
            this.mapObjects.forEach(i -> i.setRoute(null));
        }
        if (mapObjects != null) {
            mapObjects.forEach(i -> i.setRoute(this));
        }
        this.mapObjects = mapObjects;
    }

    public Route mapObjects(Set<MapObject> mapObjects) {
        this.setMapObjects(mapObjects);
        return this;
    }

    public Route addMapObjects(MapObject mapObject) {
        this.mapObjects.add(mapObject);
        mapObject.setRoute(this);
        return this;
    }

    public Route removeMapObjects(MapObject mapObject) {
        this.mapObjects.remove(mapObject);
        mapObject.setRoute(null);
        return this;
    }

    public Set<ParentObject> getPaentObjects() {
        return this.paentObjects;
    }

    public void setPaentObjects(Set<ParentObject> parentObjects) {
        if (this.paentObjects != null) {
            this.paentObjects.forEach(i -> i.setRoute(null));
        }
        if (parentObjects != null) {
            parentObjects.forEach(i -> i.setRoute(this));
        }
        this.paentObjects = parentObjects;
    }

    public Route paentObjects(Set<ParentObject> parentObjects) {
        this.setPaentObjects(parentObjects);
        return this;
    }

    public Route addPaentObject(ParentObject parentObject) {
        this.paentObjects.add(parentObject);
        parentObject.setRoute(this);
        return this;
    }

    public Route removePaentObject(ParentObject parentObject) {
        this.paentObjects.remove(parentObject);
        parentObject.setRoute(null);
        return this;
    }

    public Set<Tag> getTags() {
        return this.tags;
    }

    public void setTags(Set<Tag> tags) {
        if (this.tags != null) {
            this.tags.forEach(i -> i.setRoute(null));
        }
        if (tags != null) {
            tags.forEach(i -> i.setRoute(this));
        }
        this.tags = tags;
    }

    public Route tags(Set<Tag> tags) {
        this.setTags(tags);
        return this;
    }

    public Route addTags(Tag tag) {
        this.tags.add(tag);
        tag.setRoute(this);
        return this;
    }

    public Route removeTags(Tag tag) {
        this.tags.remove(tag);
        tag.setRoute(null);
        return this;
    }

    public Country getCountry() {
        return this.country;
    }

    public void setCountry(Country country) {
        this.country = country;
    }

    public Route country(Country country) {
        this.setCountry(country);
        return this;
    }

    public Region getRegion() {
        return this.region;
    }

    public void setRegion(Region region) {
        this.region = region;
    }

    public Route region(Region region) {
        this.setRegion(region);
        return this;
    }

    public State getState() {
        return this.state;
    }

    public void setState(State state) {
        this.state = state;
    }

    public Route state(State state) {
        this.setState(state);
        return this;
    }

    // jhipster-needle-entity-add-getters-setters - JHipster will add getters and setters here

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Route)) {
            return false;
        }
        return id != null && id.equals(((Route) o).id);
    }

    @Override
    public int hashCode() {
        // see https://vladmihalcea.com/how-to-implement-equals-and-hashcode-using-the-jpa-entity-identifier/
        return getClass().hashCode();
    }

    // prettier-ignore
    @Override
    public String toString() {
        return "Route{" +
            "id=" + getId() +
            ", title='" + getTitle() + "'" +
            ", description='" + getDescription() + "'" +
            ", routeType='" + getRouteType() + "'" +
            ", photo='" + getPhoto() + "'" +
            ", photoContentType='" + getPhotoContentType() + "'" +
            ", length=" + getLength() +
            ", looped='" + getLooped() + "'" +
            ", accessible='" + getAccessible() + "'" +
            ", complexity='" + getComplexity() + "'" +
            ", price='" + getPrice() + "'" +
            ", days=" + getDays() +
            ", hours=" + getHours() +
            ", coverage='" + getCoverage() + "'" +
            ", create='" + getCreate() + "'" +
            ", update='" + getUpdate() + "'" +
            "}";
    }
}
